
#ifndef GLOBALS_H_
#define GLOBALS_H_

#include <main.h>

//this is the number of infineon BTS72220 ICs on the board
#define MAX_INFINEON_IC 4

//this is the max number of outputs channels the infineon BFS72220 supports
#define INFINEON_CHANNELS 5

#define MAX_DIGITAL_IN 8
#define MAX_DIGITAL_OUT_CHANNELS 8

extern SPI_HandleTypeDef hspi3;

#ifdef __cplusplus


typedef enum{
	HC_Outputs_CranePower,
	HC_Outputs_StrobeLights,
	HC_Outputs_WorkLightsLeft,
	HC_Outputs_WorkLightsRight,
	HC_Outputs_CompartmentLights,
	HC_Outputs_CompressorPowerUnit,
	HC_Outputs_Aux,
	HC_Outputs_MAX,
	LC_Outputs_1,
	LC_Outputs_2,
	LC_Outputs_3,
	LC_Outputs_4,
	LC_Outputs_5,
	LC_Outputs_6,
	LC_Outputs_7,
	LC_Outputs_MAX,
	OD_Outputs_CraneLights,
	Application_Outputs_MAX
} Application_Outputs_t;

typedef enum{
	OutputType_LowSide,
	OutputType_HighSide,
	OutputType_TriState,
	OutputType_NotConfigured
}OutputType_t;

typedef enum{
	OutputState_Off,
	OutputState_On,
	OutputState_Fault,
	OutputState_MAX,
} OutputState_t;


typedef enum
{
	ButtonState_InActive = 0x00,
	ButtonState_Fault = 0x01,
	ButtonState_Active = 0x03,
} ButtonState_t;

typedef enum
{
	ButtonNames_Crane,
	ButtonNames_WorkLightLeft,
	ButtonNames_WorkLightRight,
	ButtonNames_StrobeLight,
	ButtonNames_Compressor,
	ButtonNames_DomeLight,
	ButtonNames_CraneLight,
	ButtonNames_Aux,
	ButtonNames_MAX,
} ButtonNames_t;


typedef enum{
    DigitalIn_1,
    DigitalIn_2,
    DigitalIn_3,
    DigitalIn_4,
    DigitalIn_5,
    DigitalIn_6,
    DigitalIn_7,
    DigitalIn_8,
    DigitalIn_MAX,
} DigitalInPins_t;

#include <output.h>
#include <spi.h>
#include <utilities.h>
#include <InfineonManager.h>
#include <hmiSystemsKeypad.h>
#include <FiniteStateMachine.h>
#include <ApplicationOutputs.h>

extern FiniteStateMachine* ptoStateMachine;
extern uint32_t ptoCurrentState;

class ApplicationOutputs;
extern ApplicationOutputs* appOutputs;

extern hmiSystemsKeypad* hmiKeypad;


extern uint32_t anfbCounts[MAX_INFINEON_IC];

extern uint32_t hsCurrentSenseCounts[MAX_INFINEON_IC * INFINEON_CHANNELS];





extern uint32_t digitalInCounts[MAX_DIGITAL_IN];

extern uint32_t digitalTriFlybackCounts;

extern uint32_t thermistorCounts;

extern bool canCraneActiveStatus;
extern bool canCraneEngineStop;
extern bool canCraneEngineStart;
extern bool canCraneAux;

extern bool eStopActive;

uint8_t GetDigitalInState(uint32_t counts);

#endif  // __cplusplus

#ifdef __cplusplus
extern "C" {
#endif

#include <dispatcherSetup.h>

extern ADC_HandleTypeDef hadc1;

extern CAN_HandleTypeDef hcan1;
extern CAN_HandleTypeDef hcan2;

extern TIM_HandleTypeDef htim2;

extern CRC_HandleTypeDef hcrc;

extern uint8_t controllerAddress;

extern int dispCPUUsage;        // dispatcherCore.c

void globalSetup();

#ifdef __cplusplus
}
#endif

#endif /* GLOBALS_H_ */
