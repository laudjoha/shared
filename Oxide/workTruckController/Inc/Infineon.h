
#pragma once

#include <main.h>
#include <output.h>
#include <spi.h>


class Infineon
{

public:
	Infineon(uint8_t _index);
	~Infineon() {}

	uint8_t getWriteByte();
	void setReadByte(uint8_t _readByte);

	bool getNeedResponse();

	void step();

	/*
	 * 0x00 - default, all channels operating independently
	 * 0x01 - OUT0 + OUT3 are in parallel configuration
	 * 0x02 - OUT1 + OUT2 are in parallel configuration
	 * 0x03 - OUT0 + OUT3 and OUT1 + OUT2 are in parallel configuration
	 */
	void configureParallelOutput(uint8_t _setting);

	void clearOrSetOutput(uint32_t outputIndex, uint32_t state);

	void shiftDCRMuxRegister(uint8_t position);

	void clearOutputEStop();


private:

	void (Infineon::* fsmptr)();
	void (Infineon::* callback)() = nullptr;

	//at construction, index is set to the 1 based position of this object's IC on the daisy chained spi bus
	// 1 receives MOSI directly from the uC, MAX_INFINEON_IC transmits MISO directly to the uC
	uint8_t index = 0x00;

	uint8_t writeByte = 0x00;
	uint8_t readByte = 0x00;

	uint8_t parallelConfiguration = 0x00;
	uint8_t outputConfiguration = 0x80;

	uint8_t dcrMuxConfiguration = 0x00;

	bool DCRSWRbitSet = false;
	bool needResponse = false;
	bool initComplete = false;

	void stateInitCheckWrnDiag();
	void stateInitCheckStdDiag();
	void stateInitCheckErrDiag();
	void stateInitCheckDCR();
	void stateInitConfigureHWCR();
	void stateInitSetOutputs();

	void stateConfigurePCS();
	void stateToggleDCR();

	void stateWriteOutRegister();

	void stateShiftDCRMux();

	void stateIdle();
};
