
#pragma once

#include <fsmState.h>
#include <FiniteStateMachine.h>



class ptoInit : public fsmState
{
public:

	// Inherited via fsmState
	void enter(FiniteStateMachine* fsm) override;
	void run(FiniteStateMachine* fsm) override;
	void exit(FiniteStateMachine* fsm) override;

	static fsmState& getInstance();

private:
	ptoInit() {}
};



class ptoArm : public fsmState
{
public:

	// Inherited via fsmState
	void enter(FiniteStateMachine* fsm) override;
	void run(FiniteStateMachine* fsm) override;
	void exit(FiniteStateMachine* fsm) override;

	static fsmState& getInstance();

private:
	ptoArm() {}
};



class ptoStart : public fsmState
{
public:

	// Inherited via fsmState
	void enter(FiniteStateMachine* fsm) override;
	void run(FiniteStateMachine* fsm) override;
	void exit(FiniteStateMachine* fsm) override;

	static fsmState& getInstance();

private:
	ptoStart() {}
};



class ptoRun : public fsmState
{
public:

	// Inherited via fsmState
	void enter(FiniteStateMachine* fsm) override;
	void run(FiniteStateMachine* fsm) override;
	void exit(FiniteStateMachine* fsm) override;

	static fsmState& getInstance();

private:
	ptoRun() {}
};



class ptoStandby : public fsmState
{
public:

	// Inherited via fsmState
	void enter(FiniteStateMachine* fsm) override;
	void run(FiniteStateMachine* fsm) override;
	void exit(FiniteStateMachine* fsm) override;

	static fsmState& getInstance();

private:
	ptoStandby() {}
};



class ptoRetry : public fsmState
{
public:

	// Inherited via fsmState
	void enter(FiniteStateMachine* fsm) override;
	void run(FiniteStateMachine* fsm) override;
	void exit(FiniteStateMachine* fsm) override;

	static fsmState& getInstance();

private:
	ptoRetry() {}
};



class ptoFault : public fsmState
{
public:

	// Inherited via fsmState
	void enter(FiniteStateMachine* fsm) override;
	void run(FiniteStateMachine* fsm) override;
	void exit(FiniteStateMachine* fsm) override;

	static fsmState& getInstance();

private:
	ptoFault() {}
};



class ptoShutdown : public fsmState
{
public:

	// Inherited via fsmState
	void enter(FiniteStateMachine* fsm) override;
	void run(FiniteStateMachine* fsm) override;
	void exit(FiniteStateMachine* fsm) override;

	static fsmState& getInstance();

private:
	ptoShutdown() {}
};
