
#ifndef ADC_H_
#define ADC_H_

#include "stm32f2xx_hal.h"
#ifdef __cplusplus
#include "utilities.h"

extern FilterAve adc2Ave;
#endif

#ifdef __cplusplus
extern "C" {
#endif

#define ADC1VALCHANCOUNT 100
#define ADC1VALCHANNELS  9
#define ADC1VALCOUNT (ADC1VALCHANCOUNT * ADC1VALCHANNELS)
#define ADC2VALCOUNT 100
extern uint16_t adcVals[ADC1VALCOUNT];
extern uint16_t adc2Vals[3];

extern bool adcReady;

// maximum counts for digital low
#define DIGITAL_IN_LOW_THRESH 400

// minimum counts for digital high
#define DIGITAL_IN_HI_THRESH 2800


#ifdef __cplusplus
}
#endif

#endif /* ADC_H_ */
