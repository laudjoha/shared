
#ifndef CAN_H_
#define CAN_H_

#include <main.h>

void CANInit();

class CANParameter
{
public:
    CANParameter() : val(0), lastTimeTick(0), validTimeTicks(5000) {}
    bool isValid();
    void setVal(uint8_t *data, uint8_t bytePos, uint8_t numBytes);
    uint32_t getVal() { return(val); }

private:

    uint32_t val;
    uint32_t lastTimeTick;
    uint32_t validTimeTicks;
};

#endif /* CAN_H_ */
