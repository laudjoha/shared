
#ifndef EXAMPLE_USER_DISPATCHER_H_
#define EXAMPLE_USER_DISPATCHER_H_

//#include "stm32f2xx_nucleo_144.h"
#include "stm32f2xx_hal.h"

#ifdef __cplusplus
extern "C" {
#endif

#define DISPTIME_CYCLESPERMS 120000

#define EVENT_FLAG_INCLUDE_IN_TRACE			0x00000001

typedef struct
{
	uint32_t ms;
	uint32_t cyclesPerMs;
}DispTimeSpec_t;

typedef void (*EventFunction_t)();
typedef struct
{
	int eventCount;
	int flags;
	EventFunction_t func;
	DispTimeSpec_t runTime;
	uint32_t lastPeriodRunTime;
	int qsize;
	int qptr;
	uint32_t *queue;
} DispEvent_t;

//Dispatcher Core prototypes
void dispEvent(uint32_t eventId);
void dispEventWithData(uint32_t eventId, uint32_t *data, int length);
int  dispGetQData(uint32_t eventId, uint32_t *data);
void dispExec();
void dispGetTime(DispTimeSpec_t *);
void dispDiffTime(DispTimeSpec_t *result, DispTimeSpec_t *hiVal, DispTimeSpec_t *loVal);
void dispAddTime(DispTimeSpec_t *result, DispTimeSpec_t *val1, DispTimeSpec_t *val2);
void dispMoveTimes();
void setPendSV();

//Event ID / Priority Definition
typedef enum
{
	EVENT_1MS_TIMER,
	EVENT_TIMER_SET,
    EVENT_ADC_READ,
	EVENT_ESTOP,
    EVENT_SPI_SEND,
    EVENT_SPI_TIMEOUT,
	EVENT_INFINEON_CALLBACK,
    EVENT_CAN1_RX,
    EVENT_CAN1_TX,
	EVENT_CAN2_RX,
	EVENT_CAN2_TX,
	EVENT_CAN2_TX_CONFIG,
	EVENT_CAN2_RESTARTRCV,
	EVENT_I2C_SEND,
	EVENT_I2C_TIMEOUT,
	EVENT_CAN2_PARSESYSTEM,
	EVENT_CAN2_PARSECRANE,
	EVENT_CAN2_PARSEKEYPAD,
	EVENT_OVERCURRENT_CHECK,
	EVENT_PTO_STATE_MACHINE_STEP,
	EVENT_EEHANDLER_CALLBACK,
	EVENT_EEHANDLER_WRITEWAIT,
	EVENT_EEINFO_CALLBACK,
	EVENT_EECONFIG_CALLBACK,
	EVENT_EECONFIG_WRITEWAIT,
	EVENT_EECONFIG_UPDATED,
	EVENT_CAN_ACTIVEMONITOR,
	EVENT_IDLE,		// Always lowest prio - for CPU usage
	EVENT_MAX_ID	// EVENT_MAX_ID must always be last
					// after adding an event, provide a function and add it to the eventTable in dispatcher.c
}EventId_t;

//Event function prototypes
void event1msTimer();						// timerUtils.c
void eventTimerSet();						// timerUtils.c
void eventADCRead();                        // adc.cpp
void eventEStop();							// ApplicationOutputs.cpp
void eventSPISend();                        // spi.cpp
void eventSPITimeout();                     // spi.cpp
void eventInfineonCallback();				// InfineonManager.cpp
void eventCAN1Rx();                         // canCore.cpp
void eventCAN1Tx();                         // canCore.cpp
void eventCAN2Rx();							// canCore.cpp
void eventCAN2Tx();							// canCore.cpp
void eventCAN2TxConfig();					// canCore.cpp
void eventCAN2RestartIT();					// canCore.cpp
void eventI2CSend();						// i2c.cpp
void eventI2CTimeout();						// i2c.cpp
void eventCANParseSystem();                 // can.cpp
void eventCANParseCrane();					// can.cpp
void eventCANParseKeypad();					// can.cpp
void eventOvercurrentCheck();				// ApplicationOutputs.cpp
void eventPtoStateMachineStep();			// FiniteStateMachine.cpp
void eventEEHandlerCallback();              // eeprom.cpp
void eeHandlerWriteWait();                  // eeprom.ccp
void eventEEInfoCallback();                 // eeprom.cpp
void eventEEConfigCallback();               // eeprom.cpp
void eventEEConfigWriteWait();				// eeprom.cpp
void eventEEConfigUpdated();                  // global.cpp
void eventCANActiveMonitor();               // canCore.cpp
void eventIdle();							// main.c

#ifdef __cplusplus
}
#endif

#endif /* EXAMPLE_USER_DISPATCHER_H_ */
