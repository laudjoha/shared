
#pragma once

#include <globals.h>
#include <InfineonManager.h>
#include <output.h>


class ApplicationOutputs
{
public:

	ApplicationOutputs();
	~ApplicationOutputs();

	void ShiftCurrentSenseOutput(uint8_t position);

	void currentLimitCompare(uint32_t infineonICIndex, uint32_t infineonCurrentSenseMuxPosition, uint32_t measuredCurrent);
	void updateCurrentLimit(Application_Outputs_t output, uint8_t limit);

	//firmware is not currently written to support switching from active low side to active high side
	// if you need this behavior, you will need to modify OutputSet_Hi and OutputSet_Lo to account for switch timing
	void LC_configure(uint32_t channel, OutputType_t outputType);
	OutputType_t LC_getConfiguredType(uint32_t channel);

	OutputState_t getOutputState(Application_Outputs_t output);

	void OutputSet_Hi(Application_Outputs_t output);
	void OutputSet_Lo(Application_Outputs_t output);

	void OD_setHi(uint32_t channel);
	void OD_setLo(uint32_t channel);

	void applicationEStop();

	uint32_t getOutputCurrent(Application_Outputs_t output);

private:
	InfineonManager* hsOuts = NULL;
	DigitalOutput* lsOuts[MAX_DIGITAL_OUT_CHANNELS];
	OutputType_t lcOutputConfiguredType[MAX_DIGITAL_OUT_CHANNELS] = {OutputType_NotConfigured};

	OutputState_t ApplicationOutputState[Application_Outputs_MAX] = {OutputState_Off};
	uint32_t ApplicationOutputFaultCurrentCounts[Application_Outputs_MAX] = {0};

	uint32_t getICIndex(Application_Outputs_t output);
	uint32_t getICOutput(Application_Outputs_t output);
	uint32_t getOutputIndex(Application_Outputs_t output);
	Application_Outputs_t getApplicationOutput(uint32_t infineonICIndex, uint32_t infineonICChannel);
};
