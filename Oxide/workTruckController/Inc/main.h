/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f2xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define CAN2_STB_Pin GPIO_PIN_2
#define CAN2_STB_GPIO_Port GPIOE
#define VINFault_Pin GPIO_PIN_3
#define VINFault_GPIO_Port GPIOE
#define LC_OUT_OD_6_uP_Pin GPIO_PIN_5
#define LC_OUT_OD_6_uP_GPIO_Port GPIOE
#define LC_OUT_OD_7_uP_Pin GPIO_PIN_6
#define LC_OUT_OD_7_uP_GPIO_Port GPIOE
#define VBATT_SENSE_Pin GPIO_PIN_0
#define VBATT_SENSE_GPIO_Port GPIOC
#define ANFB1_uP_Pin GPIO_PIN_2
#define ANFB1_uP_GPIO_Port GPIOA
#define ANFB2_uP_Pin GPIO_PIN_3
#define ANFB2_uP_GPIO_Port GPIOA
#define HW_Version_Pin GPIO_PIN_4
#define HW_Version_GPIO_Port GPIOA
#define ANFB3_uP_Pin GPIO_PIN_5
#define ANFB3_uP_GPIO_Port GPIOA
#define DIG_TRI_INPUT1_uP_Pin GPIO_PIN_6
#define DIG_TRI_INPUT1_uP_GPIO_Port GPIOA
#define DIG_TRI_INPUT2_uP_Pin GPIO_PIN_7
#define DIG_TRI_INPUT2_uP_GPIO_Port GPIOA
#define ANFB4_uP_Pin GPIO_PIN_4
#define ANFB4_uP_GPIO_Port GPIOC
#define DIG_TRI_MUXOUT_uP_Pin GPIO_PIN_5
#define DIG_TRI_MUXOUT_uP_GPIO_Port GPIOC
#define DIG_TRI_SELECT_2_uP_Pin GPIO_PIN_9
#define DIG_TRI_SELECT_2_uP_GPIO_Port GPIOE
#define OD_OUT1_uP_Pin GPIO_PIN_13
#define OD_OUT1_uP_GPIO_Port GPIOE
#define DIAG_LED_Pin GPIO_PIN_10
#define DIAG_LED_GPIO_Port GPIOB
#define LC_OUT_OD_1_uP_Pin GPIO_PIN_11
#define LC_OUT_OD_1_uP_GPIO_Port GPIOB
#define EEPROM_WP_Pin GPIO_PIN_12
#define EEPROM_WP_GPIO_Port GPIOB
#define LC_OUT_OD_2_uP_Pin GPIO_PIN_12
#define LC_OUT_OD_2_uP_GPIO_Port GPIOD
#define LC_OUT_OD_3_uP_Pin GPIO_PIN_13
#define LC_OUT_OD_3_uP_GPIO_Port GPIOD
#define LC_OUT_OD_4_uP_Pin GPIO_PIN_14
#define LC_OUT_OD_4_uP_GPIO_Port GPIOD
#define LC_OUT_OD_5_uP_Pin GPIO_PIN_15
#define LC_OUT_OD_5_uP_GPIO_Port GPIOD
#define DIG_TRI_SELECT_0_uP_Pin GPIO_PIN_6
#define DIG_TRI_SELECT_0_uP_GPIO_Port GPIOC
#define SPI_CS_Pin GPIO_PIN_7
#define SPI_CS_GPIO_Port GPIOC
#define DIG_TRI_SELECT_1_uP_Pin GPIO_PIN_8
#define DIG_TRI_SELECT_1_uP_GPIO_Port GPIOC
#define BOOT0Charge_Pin GPIO_PIN_8
#define BOOT0Charge_GPIO_Port GPIOA
#define CAN1_STB_Pin GPIO_PIN_3
#define CAN1_STB_GPIO_Port GPIOD
#define HALLSENSOR_Pin GPIO_PIN_6
#define HALLSENSOR_GPIO_Port GPIOD
#define CAN2_EN_Pin GPIO_PIN_0
#define CAN2_EN_GPIO_Port GPIOE
#define CAN2_ERR_Pin GPIO_PIN_1
#define CAN2_ERR_GPIO_Port GPIOE
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
