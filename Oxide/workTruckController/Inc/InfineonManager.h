
#pragma once

#include <main.h>
#include <output.h>
#include <spi.h>
#include <string.h>
#include <dispatcherSetup.h>
#include <globals.h>

#include <Infineon.h>


class InfineonManager
{
public:
	DigitalOutput* cs{};


	InfineonManager(SPI *hspi_, DigitalOutput *cs_);

	~InfineonManager();

	//zero based indexes for IC and output on the IC
	void setOutput(uint32_t index_IC, uint32_t index_output);
	void clearOutput(uint32_t index_IC, uint32_t index_output);

	//turn off all outputs
	void eStopShutdown();

	uint32_t getCurrentLimit(uint32_t index_IC, uint32_t index_output);
	void setCurrentLimit(uint32_t index_IC, uint32_t index_output, uint8_t limit);

	void shiftDCRMux(uint8_t position);

	void spiDone();


private:
	InfineonManager(){}

	uint32_t InfineonCurrentLimit_LowCurrentSeries = 2805; // current counts = 544 * x + 85.1
	uint32_t InfineonCurrentLimit_HighCurrentSeries = 2000; //not tested
	uint32_t InfineonCurrentLimit_ExternalFET = 978; // current counts = 65.1 * x + 1.63
	uint32_t InfineonCurrentLimit_LowCurrentParallel = 2000; //not tested
	uint32_t InfineonCurrentLimit_HighCurrentParallel = 1978; // current counts = 130 * x + 27.4

	uint32_t currentLimits[MAX_INFINEON_IC * INFINEON_CHANNELS] =
	{
		InfineonCurrentLimit_HighCurrentParallel,	//IC1 out0
		InfineonCurrentLimit_LowCurrentSeries,		//IC1 out1
		InfineonCurrentLimit_LowCurrentSeries,		//IC1 out2
		InfineonCurrentLimit_HighCurrentParallel,	//IC1 out3
		InfineonCurrentLimit_ExternalFET,			//IC1 ext
		InfineonCurrentLimit_HighCurrentParallel,	//IC2 out0
		InfineonCurrentLimit_LowCurrentSeries,		//IC2 out1
		InfineonCurrentLimit_LowCurrentSeries,		//IC2 out2
		InfineonCurrentLimit_HighCurrentParallel,	//IC2 out3
		InfineonCurrentLimit_ExternalFET,			//IC2 ext
		InfineonCurrentLimit_HighCurrentParallel,	//IC3 out0
		InfineonCurrentLimit_LowCurrentSeries,		//IC3 out1
		InfineonCurrentLimit_LowCurrentSeries,		//IC3 out2
		InfineonCurrentLimit_HighCurrentParallel,	//IC3 out3
		InfineonCurrentLimit_ExternalFET,			//IC3 ext
		InfineonCurrentLimit_HighCurrentParallel,	//IC4 out0
		InfineonCurrentLimit_LowCurrentSeries,		//IC4 out1
		InfineonCurrentLimit_LowCurrentSeries,		//IC4 out2
		InfineonCurrentLimit_HighCurrentParallel,	//IC4 out3
		InfineonCurrentLimit_ExternalFET,			//IC4 ext
	};

	bool isWrite = false;
	bool getResponse = true;

	SPI* hspi{};

	Infineon* digitalOutICs[MAX_INFINEON_IC];

	uint8_t wbuf[MAX_INFINEON_IC];
	uint8_t rbuf[MAX_INFINEON_IC];

	void BuildWriteBuffer();
	void processResponse();

	void InitializeICs();
	void ConfigureParallelOutput();
};
