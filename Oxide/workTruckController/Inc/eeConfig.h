
#ifndef EECONFIG_H_
#define EECONFIG_H_

#include <eeprom.h>

///////////////////////////////////////////////////////////////////////////////
// EEConfig
///////////////////////////////////////////////////////////////////////////////
#define EE_CONFIG_VERFSION 1
#define EE_COUNTS_PER_CLICK 50

class EEConfig : public EELayout
{
    typedef struct
    {
        // core
        uint8_t  cfgVersion;
        uint16_t cfgCommonCANTimeout;

        // specific to ACPDM
        uint16_t cfgCompressorTimeout;
        uint16_t cfgCraneTimeout;
        uint16_t cfgIgnitionActTime;
        uint32_t cfgHCCurrentLimits;        // nibble packed

    } EECfgData;
    typedef struct
    {
        EELayoutHeader_t head;
        EECfgData        data;
    } EECfgLayout_t;

public:
    EEConfig();
    ~EEConfig();

    // data accessors
    uint16_t getCfgCommonCANTimeout() { return(cfgData.cfgCommonCANTimeout); }

    uint16_t getCfgCompressorTimeout() { return(cfgData.cfgCompressorTimeout); }
    uint16_t getCfgCraneTimeout()      { return(cfgData.cfgCraneTimeout); }
    uint16_t getCfgIgnitionActTime()   { return(cfgData.cfgIgnitionActTime); }
    uint8_t  getCfgHCCurrentLimit(int idx);

    void setCfgCommonCANTimeout(uint16_t val) { cfgData.cfgCommonCANTimeout = val; }

    void setCfgCompressorTimeout(uint16_t val) { cfgData.cfgCompressorTimeout = val; }
    void setCfgCraneTimeout(uint16_t val)      { cfgData.cfgCraneTimeout = val; }
    void setCfgIgnitionActTime(uint16_t val)   { cfgData.cfgIgnitionActTime = val; }
    void setCfgHCCurrentLimit(int idx, uint16_t val);

    bool getCfgDataGood() { return(cfgDataGood); }
    bool readIsComplete() { return(readComplete); }


    // write config data
    void writeBrdCfg();
    void writeBrdCfg1();
    void writeBrdCfg2();
    void writeBrdCfg3();
    void writeBrdCfg4();
    void writeBrdCfg5();

    void readCfg();
    void readCfg10();

    void setToDefaults();
    void doNextState();
    void reset();
    bool hasHeader();

    int  getBlockSize() { return(blockSize); }
    void *getBlockPtr() { return(block); }
    int  getBlockOffset() { return(blockOffset); }
    void setErrorCode(int ec) { errorCode = ec; }
    string getErrorMsg() { return(lastErrMsg); }

    int getState() { return(state); }

    bool hasChanged() { return(changed); }
    void clearChanged() { changed = false; }

private:
    EECfgData cfgData;
    bool     cfgDataGood;
    int      blockOffset;   //current address offset...we write 3 copies of the data
    const int baseOffset;   //base address for this data in EEPROM
    int      errorCode;
    int      state;
    uint8_t  *block;
    int      blockSize;
    string   lastErrMsg;
    bool     busy;
    bool     changed;
    bool     readComplete;
};


extern EEConfig eeCfgObj;

#endif /* EECONFIG_H_ */
