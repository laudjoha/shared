
#pragma once

#include <fsmState.h>

class FiniteStateMachine
{

private:
	fsmState* currentState;

public:
	FiniteStateMachine();

	inline fsmState* getCurrentState() const { return currentState; }

	void step();
	void setState(fsmState& newState);

};
