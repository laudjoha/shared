
#pragma once

/*
 * Abstract interface class for state functions
 *
 * 	Enter:	Anything that needs to be done in a state before evaluating which state to move to.
 * 			Called immediately after new state is assigned.
 *
 * 	Run:	Make evaluations to determine next state. Assign the next state.
 * 			Runs on 50mS timer.
 *
 * 	Exit:	Anything that needs to be done after the state transition evaluation is made.
 * 			Called 50mS after new state is assigned.
 *
 * 			REE---50mS---REE
 * 			uxn          uxn
 * 			nit          nit
 * 			 te           te
 *			  r            r
 */

class FiniteStateMachine; //forward declare to resolve circular dependency/include

class fsmState
{

public:
	virtual void enter(FiniteStateMachine* fsm) = 0;
	virtual void run(FiniteStateMachine* fsm) = 0;
	virtual void exit(FiniteStateMachine* fsm) = 0;

	virtual ~fsmState(){}

};
