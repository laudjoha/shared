
#pragma once

#include <main.h>
#include <canCore.h>


/*
 *
 *       ___ ___ ___ ___
 *		|   |   |   |   |
 *		| 1 | 2 | 3 | 4 |
 *		|___|___|___|___|
 *		|   |   |   |   |
 *	    | 5 | 6 | 7 | 8 |
 *		|___|___|___|___|
 *
 * 		Button	Description					IO
 * 		1		Crane Arm					HC Output 1
 * 		2		Left Light					HC Output 3
 * 		3		Right Light					HC Output 4
 * 		4 		Strobe Light				HC Output 2
 *
 * 		5		Reciprocating Compressor	HC Output 6
 * 		6		Dome Light					HC Output 5 (dome light = compartment lights?)
 * 		7		Crane Light					OD 1
 * 		8		Aux							HC Output 7
 *
 *
 */



class hmiSystemsKeypad
{

public:
	hmiSystemsKeypad(uint8_t _sourceAddress, uint8_t _rows, uint8_t _columns);

	~hmiSystemsKeypad();

	void ParseKeypadMessage(unsigned char data[]);
	void UpdateKeypadLights();
	CANEventMsg_t GetTxMessage();

	ButtonState_t GetButtonState(ButtonNames_t button);

	void SetLEDState(Application_Outputs_t output, ButtonState_t state);

	void AuxillaryButtonPress(ButtonNames_t button);


private:
	hmiSystemsKeypad(){}

	const uint32_t txPGN = 0x1F20E;
	const uint32_t rxPGN = 0x1F20D;

	uint8_t sourceAddress = 0;
	uint8_t rows = 0;
	uint8_t columns = 0;

	CANEventMsg_t txMessage;

	void ToggleButton(uint8_t buttonIndex);
	void SetButtonState(ButtonNames_t button, ButtonState_t state);

	ButtonNames_t IndexToName(uint8_t buttonIndex);
	uint8_t NameToIndex(ButtonNames_t buttonName);

	std::vector<ButtonState_t> prevButtonState;
	std::vector<ButtonState_t> buttonState;
};
