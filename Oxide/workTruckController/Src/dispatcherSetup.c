
//#include <PDMOutput.h>
#include "dispatcherSetup.h"
#include "timerUtils.h"
#include "canCore.h"
#include "spi.h"
#include "i2c.h"
#include "eeprom.h"

// Pre-defined event queues
	//EVENT_1MS_TIMER
	//EVENT_TIMER_SET
uint32_t evqTimerSet[TIMER_QSIZE];
    //EVENT_ADC_READ,
	//EVENT_ESTOP
    //EVENT_SPI_SEND
uint32_t evqSPISend[SPI_QSIZE];
    //EVENT_SPI_TIMEOUT
	//EVENT_INFINEON_CALLBACK
uint32_t evqInfineonCB[sizeof(void *)];
    //EVENT_CAN1_RX
uint32_t evqCAN1Rx[CAN_RX_QSIZE];
    //EVENT_CAN1_TX
uint32_t evqCAN1Tx[CAN_TX_QSIZE];
	//EVENT_CAN2_RX
uint32_t evqCAN2Rx[CAN_RX_QSIZE];
	//EVENT_CAN2_TX
uint32_t evqCAN2Tx[CAN_TX_QSIZE];
	//EVENT_CAN2_TX_CONFIG
	//EVENT_CAN2_RESTARTRCV
	//EVENT_I2C_SEND
uint32_t evqI2CSend[I2C_QSIZE];
	//EVENT_I2C_TIMEOUT
    //EVENT_CAN2_PARSESYSTEM
uint32_t evqCANParseSystem[sizeof(CANEventMsg_t)/sizeof(uint32_t)];
	//EVENT_CAN2_PARSECRANE
uint32_t evqCANParseCrane[sizeof(CANEventMsg_t)/sizeof(uint32_t)];
	//EVENT_CAN2_PARSEKEYPAD
uint32_t evqCANParseKeypad[sizeof(CANEventMsg_t)/sizeof(uint32_t)];
	//EVENT_OVERCURRENT_CHECK
	//EVENT_PTO_STATE_MACHINE_STEP
    //EVENT_EEHANDLER_CALLBACK
uint32_t evqEEHandler[EE_HANDLER_QSIZE];
    //EVENT_EEHANDLER_WRITEWAIT
uint32_t evqEEHandlerWait[EE_HANDLER_WAIT_QSIZE];
    //EVENT_EEINFO_CALLBACK
uint32_t evqEEInfo[EE_QSIZE];
    //EVENT_EECONFIG_CALLBACK
uint32_t evqEEConfig[EE_QSIZE];
	//EVENT_EECONFIG_WRITEWAIT
uint32_t evqEEConfigWait[EE_HANDLER_WAIT_QSIZE];
    //EVENT_EECONFIG_UPDATED
    //EVENT_CAN_ACTIVEMONITOR
	//EVENT_IDLE

DispEvent_t eventTable[] = {
	//EVENT_1MS_TIMER,
		{	0, 0, event1msTimer, {0,0}, 0, 0, 0, 0 },
	//EVENT_TIMER_SET,
		{	0, 0, eventTimerSet, {0,0}, 0, sizeof(evqTimerSet)/sizeof(uint32_t), 0, evqTimerSet },
    //EVENT_ADC_READCURRENTS
        {   0, EVENT_FLAG_INCLUDE_IN_TRACE, eventADCRead, {0,0}, 0, 0, 0, 0 },
	//EVENT_ESTOP
			{	0, EVENT_FLAG_INCLUDE_IN_TRACE, eventEStop, {0,0}, 0, 0, 0, 0},
    //EVENT_SPI_SEND
        {   0, EVENT_FLAG_INCLUDE_IN_TRACE, eventSPISend, {0,0}, 0, sizeof(evqSPISend)/sizeof(uint32_t), 0, evqSPISend },
    //EVENT_SPI_TIMEOUT
        {   0, EVENT_FLAG_INCLUDE_IN_TRACE, eventSPITimeout, {0,0}, 0, 0, 0, 0 },
	//EVENT_INFINEON_CALLBACK
		{   0, EVENT_FLAG_INCLUDE_IN_TRACE, eventInfineonCallback, {0,0}, 0, sizeof(evqInfineonCB)/sizeof(uint32_t), 0, evqInfineonCB },
    //EVENT_CAN1_RX,
        {   0, EVENT_FLAG_INCLUDE_IN_TRACE, eventCAN1Rx, {0,0}, 0, sizeof(evqCAN1Rx)/sizeof(uint32_t), 0, evqCAN1Rx },
    //EVENT_CAN1_TX,
        {   0, EVENT_FLAG_INCLUDE_IN_TRACE, eventCAN1Tx, {0,0}, 0, sizeof(evqCAN1Tx)/sizeof(uint32_t), 0, evqCAN1Tx },
	//EVENT_CAN2_RX,
		{   0, EVENT_FLAG_INCLUDE_IN_TRACE, eventCAN2Rx, {0,0}, 0, sizeof(evqCAN2Rx)/sizeof(uint32_t), 0, evqCAN2Rx },
	//EVENT_CAN2_TX,
		{   0, EVENT_FLAG_INCLUDE_IN_TRACE, eventCAN2Tx, {0,0}, 0, sizeof(evqCAN2Tx)/sizeof(uint32_t), 0, evqCAN2Tx },
	//EVENT_CAN2_TX_CONFIG
		{   0, EVENT_FLAG_INCLUDE_IN_TRACE, eventCAN2TxConfig, {0,0}, 0, 0, 0, 0 },
	//EVENT_CAN2_RESTARTRCV
		{	0, EVENT_FLAG_INCLUDE_IN_TRACE, eventCAN2RestartIT, {0,0}, 0, 0, 0, 0},
	//EVENT_I2C_SEND
		{   0, EVENT_FLAG_INCLUDE_IN_TRACE, eventI2CSend, {0,0}, 0, sizeof(evqI2CSend)/sizeof(uint32_t), 0, evqI2CSend },
	//EVENT_I2C_TIMEOUT
		{   0, EVENT_FLAG_INCLUDE_IN_TRACE, eventI2CTimeout, {0,0}, 0, 0, 0, 0 },
	//EVENT_CAN2_PARSESYSTEM
        {   0, EVENT_FLAG_INCLUDE_IN_TRACE, eventCANParseSystem, {0,0}, 0, sizeof(evqCANParseSystem)/sizeof(uint32_t), 0, evqCANParseSystem },
	//EVENT_CAN2_PARSECRANE
		{   0, EVENT_FLAG_INCLUDE_IN_TRACE, eventCANParseCrane, {0,0}, 0, sizeof(evqCANParseCrane)/sizeof(uint32_t), 0, evqCANParseCrane },
	//EVENT_CAN2_PARSEKEYPAD
		{   0, EVENT_FLAG_INCLUDE_IN_TRACE, eventCANParseKeypad, {0,0}, 0, sizeof(evqCANParseKeypad)/sizeof(uint32_t), 0, evqCANParseKeypad },
	//EVENT_OVERCURRENT_CHECK
		{	0, EVENT_FLAG_INCLUDE_IN_TRACE, eventOvercurrentCheck, {0,0}, 0, 0, 0, 0},
	//EVENT_PTO_STATE_MACHINE_STEP
		{	0, EVENT_FLAG_INCLUDE_IN_TRACE, eventPtoStateMachineStep, {0,0}, 0, 0, 0, 0},
    //EVENT_EEHANDLER_CALLBACK
		{   0, EVENT_FLAG_INCLUDE_IN_TRACE, eventEEHandlerCallback, {0,0}, 0, sizeof(evqEEHandler)/sizeof(uint32_t), 0, evqEEHandler },
	//EVENT_EEHANDLER_WRITEWAIT
        {   0, EVENT_FLAG_INCLUDE_IN_TRACE, eeHandlerWriteWait, {0,0}, 0, sizeof(evqEEHandlerWait)/sizeof(uint32_t), 0, evqEEHandlerWait },
	//EVENT_EEINFO_CALLBACK
        {   0, EVENT_FLAG_INCLUDE_IN_TRACE, eventEEInfoCallback, {0,0}, 0, sizeof(evqEEInfo)/sizeof(uint32_t), 0, evqEEInfo },
	//EVENT_EECONFIG_CALLBACK
        {   0, EVENT_FLAG_INCLUDE_IN_TRACE, eventEEConfigCallback, {0,0}, 0, sizeof(evqEEConfig)/sizeof(uint32_t), 0, evqEEConfig },
	//EVENT_EECONFIG_WRITEWAIT
        {   0, EVENT_FLAG_INCLUDE_IN_TRACE, eventEEConfigWriteWait, {0,0}, 0, sizeof(evqEEConfigWait)/sizeof(uint32_t), 0, evqEEConfigWait },
    //EVENT_EECONFIG_UPDATED
        {   0, EVENT_FLAG_INCLUDE_IN_TRACE, eventEEConfigUpdated, {0,0}, 0, 0, 0, 0},
	//EVENT_CAN_ACTIVEMONITOR
		{   0, EVENT_FLAG_INCLUDE_IN_TRACE, eventCANActiveMonitor, {0,0}, 0, 0, 0, 0},
	//EVENT_IDLE
		{   0, EVENT_FLAG_INCLUDE_IN_TRACE, eventIdle, {0,0}, 0, 0, 0, 0},
	//EVENT_MAX_ID
		{	0, 0, 0, {0,0}, 0, 0, 0, 0 }
};

