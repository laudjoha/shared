
#include <FiniteStateMachine.h>
#include <PtoStates.h>

#include <globals.h>
#include <adc.h>


FiniteStateMachine::FiniteStateMachine()
{
	currentState = &ptoInit::getInstance();
}

void FiniteStateMachine::step()
{

	if(eStopActive)
	{
		setState(ptoShutdown::getInstance());
		hmiKeypad->UpdateKeypadLights();

		return;
	}

	if(!adcReady)
	{
		//need to cycle through all inputs once before controlling outputs with state machine
		return;
	}

	// 0 - PTO engage hi
	// 3 - PTO engage lo
	if(digitalInCounts[0] > DIGITAL_IN_HI_THRESH &&
	   digitalInCounts[3] < DIGITAL_IN_LOW_THRESH 	)
	{
		appOutputs->OutputSet_Hi(LC_Outputs_3); //set pto engage output high
	}
	else
	{
		appOutputs->OutputSet_Lo(LC_Outputs_3); //set pto engage output low
	}


	//if compressor demand is high, turn on flow diverter
	if(digitalTriFlybackCounts > DIGITAL_IN_HI_THRESH)
	{
		appOutputs->OutputSet_Hi(LC_Outputs_6);
	}
	else
	{
		appOutputs->OutputSet_Lo(LC_Outputs_6);
	}


	//if crane is off and if remote cradle is not ground or Vbat, turn on the chime
	if(hmiKeypad->GetButtonState(ButtonNames_Crane) == ButtonState_InActive)
	{
		bool remoteCradle = (digitalInCounts[4] < DIGITAL_IN_LOW_THRESH) || (digitalInCounts[4] > DIGITAL_IN_HI_THRESH);
		if(!remoteCradle)
		{
			appOutputs->OutputSet_Hi(LC_Outputs_7);
		}
		else
		{
			appOutputs->OutputSet_Lo(LC_Outputs_7);
		}
	}
	else
	{
		appOutputs->OutputSet_Lo(LC_Outputs_7);
	}

	currentState->run(this);
}

void FiniteStateMachine::setState(fsmState& newState)
{
	currentState->exit(this);
	currentState = &newState;
	currentState->enter(this);
}



/************ ApplicationOutputs C Functions **************************************************/
/*******************************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif

void eventPtoStateMachineStep()
{
	ptoStateMachine->step();
}

#ifdef __cplusplus
}
#endif
