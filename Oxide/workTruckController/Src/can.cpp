
#include <can.h>
#include <globals.h>
#include <canCore.h>
#include <dispatcherSetup.h>
#include <timerUtils.h>
#include <eeConfig.h>
#include <utilities.h>
#include <cstring>

/************* misc definitions *************************************/

/************* filter lists *****************************************/

// callback prototypes
void CAN2ef38(CANEventMsg_t *msg);
void CAN21f20d(CANEventMsg_t *msg);
void CAN2ffaa(CANEventMsg_t *msg);

/**** Rules on Filter Lists
 *   - No limits enforced on the number in a list, but be responsible
 *   - Each list must end with an id of 0xffffffff
 *   - callback function must be supplied and must be the correct type
 *      void (*funcName)(CANEventMsg_t *msg)
 *   - the CAN ID in the list is expected to already be masked - makes the code more efficient
 *   - search for an id in the list is currently done sequentially, put the highest rate id's toward the top of the list
 *       if known
 *
 *   Guidelines
 *   - there are 28 filters - canCore reserves 14 for each port (though the hardware does not require 14 - read the ref manual)
 *   - grouped ids in a list get turned into 1 filter, but that can let unwanted ids in - just a little extra CPU
 *   - lists can have only one id if you wish
*****/

const CANIdDefinition_t CAN2IDList1[] =
{
    //  can ID      mask        callback function
    { 0x00ef3800, 0x01ffff00, CAN2ef38 },
	{ 0x01f20d00, 0x01ffff00, CAN21f20d},
	{ 0x00ffaa00, 0x01ffff00, CAN2ffaa },
    { 0xffffffff, 0xfffffff, NULL}
};



/************* utility function *************************************/
void CANInit()
{
    CANCoreInit();
    /****
    if (CANPort1 != NULL)
    {
        CANPort1->start();
        // setIDList(Filter Bank Num, list pointer, list count)
        CANPort1->setIDList(0, CAN1IDList1, sizeof(CAN1IDList1)/sizeof(CANIdDefinition_t));
        CANPort1->setIDList(1, CAN1IDList2, sizeof(CAN1IDList2)/sizeof(CANIdDefinition_t));
        CANPort1->enableRx();
    }
    ****/
    if (CANPort2 != NULL)
    {
        CANPort2->start();
        // start CAN 2 at filter # 14 (there are 28 total)
        CANPort2->setIDList(14, CAN2IDList1, sizeof(CAN2IDList1)/sizeof(CANIdDefinition_t));
        CANPort2->enableRx();
        CANPort2->setKeepAwake();
    }

    timerSet(EVENT_CAN2_TX_CONFIG, 20*TIMER_TICKSPERMS, TIMER_FLAG_RECURRING, 0);
    timerSet(EVENT_CAN_ACTIVEMONITOR, 1*TIMER_TICKSPERSECOND, TIMER_FLAG_RECURRING, 0);
}

/************* CAN Receive functions ********************************/
void CAN2ef38(CANEventMsg_t *msg)
{
    dispEventWithData(EVENT_CAN2_PARSESYSTEM, (uint32_t *)msg, sizeof(*msg));
}

void CAN21f20d(CANEventMsg_t *msg)
{
	dispEventWithData(EVENT_CAN2_PARSEKEYPAD, (uint32_t *)msg, sizeof(*msg));
}

void CAN2ffaa(CANEventMsg_t *msg)
{
	dispEventWithData(EVENT_CAN2_PARSECRANE, (uint32_t *)msg, sizeof(*msg));
}


/************* N2K xmit functions ***********************************/
#ifdef USE_N2K
void CANN2KSendFastPacket(uint32_t canid, uint8_t seq, uint8_t *data, uint32_t sz)
{
    if ((sz > ((30*7)+6)) || (sz == 0)) return;

    CANEventMsg_t msg;
    msg.canId = canid;
    msg.dlc   = 8;
    msg.isExt = 1;

    // 1st packet
    uint8_t frame = 0;
    uint8_t cpcnt = (sz >= 6) ? 6 : sz;
    msg.data[0] = (seq << 5);
    msg.data[1] = sz;
    if (cpcnt < 6)
    {
        memset(&(msg.data[2]), 0xff, 6);
    }
    memcpy(&(msg.data[2]), data, cpcnt);
    dispEventWithData(EVENT_CAN2_TX, (uint32_t *)&msg, sizeof(msg));

    // send rest of packets
    uint8_t pnt = cpcnt;
    while (pnt < sz)
    {
        ++frame;
        msg.data[0] &= 0xe0;
        msg.data[0] += frame;
        cpcnt = ((sz - pnt) >= 7) ? 7 : (sz - pnt);
        if (cpcnt < 7)
        {
            memset(&(msg.data[1]), 0xff, 7);
        }
        memcpy(&(msg.data[1]), &(data[pnt]), cpcnt);
        dispEventWithData(EVENT_CAN2_TX, (uint32_t *)&msg, sizeof(msg));
        pnt += cpcnt;
    }
}

void CANN2K_FastPkt_SetData(uint8_t *data,
                            CANParameter *param,
                            float rawScale,
                            float rawOffset,
                            uint8_t startByte,
                            uint8_t numBytes,
                            float raw2n2kConv,
                            float raw2n2kConvOffset,
                            float n2kScale,
                            float n2kOffset,
                            uint8_t byteMask,
                            bool  isSigned)
{
    if (param->isValid() && (numBytes >= 1) && (numBytes <= 4))
    {
        --startByte;
        float fval = (param->getVal() * rawScale) + rawOffset;
        fval = (fval * raw2n2kConv) + raw2n2kConvOffset;
        fval = (fval - n2kOffset) / n2kScale;
        uint32_t val;
        if (isSigned)
        {
            int32_t sval = (int32_t)(fval+0.5);
            val = (uint32_t)sval;
        }
        else
        {
            val = (uint32_t)(fval+0.5);
        }
        if (numBytes == 1) val = val & byteMask;
        uint8_t *d = &(data[startByte]);
        for (int i = 0; i < numBytes; ++i)
        {
            d[i] = ((uint8_t *)(&val))[i];
        }
    }
}
#endif // USE_N2K


/************* event functions **************************************/
#ifdef __cplusplus
extern "C" {
#endif

const uint32_t CANBaseAddress = 0x18efff38;
void eventCAN2TxConfig()
{
    static uint8_t count = 0;

    CANEventMsg_t msg;
    msg.canId   = CANBaseAddress + controllerAddress;
    msg.isExt   = 1;

    ++count;
    if (count == 50 )
    {
        CANCoreSendStandardXmits(&msg);
        //count = 0;

        if (eeCfgObj.getCfgDataGood() && ConfigLocker.isUnlocked())
        {
            msg.data[0] = 'g';
            msg.data[1] = 1;    // Compressor Timeout
            *((uint16_t *)(&msg.data[2])) = eeCfgObj.getCfgCompressorTimeout();
            dispEventWithData(EVENT_CAN2_TX, (uint32_t *)&msg, sizeof(msg));

            msg.data[1] = 2;    // Crane timeout
            *((uint16_t *)(&msg.data[2])) = eeCfgObj.getCfgCraneTimeout();
            dispEventWithData(EVENT_CAN2_TX, (uint32_t *)&msg, sizeof(msg));

            msg.data[1] = 3;    // Ignition activate time
            *((uint16_t *)(&msg.data[2])) = eeCfgObj.getCfgIgnitionActTime();
            dispEventWithData(EVENT_CAN2_TX, (uint32_t *)&msg, sizeof(msg));

            for (int i = 0; i < 8; ++i)
            {
                msg.data[1] = 4+i;    // Ignition activate time
                *((uint16_t *)(&msg.data[2])) = eeCfgObj.getCfgHCCurrentLimit(i);
                dispEventWithData(EVENT_CAN2_TX, (uint32_t *)&msg, sizeof(msg));
            }
        }


        msg.dlc = 3;
        msg.data[0] = 'x';
        msg.data[1] = 0x00;
        msg.data[2] = ptoCurrentState;
        //memset(&(msg.data[3]), 0, 5);
        dispEventWithData(EVENT_CAN2_TX, (uint32_t *)&msg, sizeof(msg));

        msg.dlc = 5;
        msg.data[0] = 'x';
        msg.data[1] = 0x01;

        uint8_t tempByte = GetDigitalInState(digitalInCounts[0]);
        tempByte |= (GetDigitalInState(digitalInCounts[1]) << 2);
        tempByte |= (GetDigitalInState(digitalInCounts[2]) << 4);
        tempByte |= (GetDigitalInState(digitalInCounts[3]) << 6);

        msg.data[2] = tempByte;

        tempByte = GetDigitalInState(digitalInCounts[4]);
		tempByte |= (GetDigitalInState(digitalInCounts[5]) << 2);
		tempByte |= (GetDigitalInState(digitalInCounts[6]) << 4);
		tempByte |= (GetDigitalInState(digitalInCounts[7]) << 6);

		msg.data[3] = tempByte;

		msg.data[4] = GetDigitalInState(digitalTriFlybackCounts);

		dispEventWithData(EVENT_CAN2_TX, (uint32_t *)&msg, sizeof(msg));


		msg.dlc = 4;
		msg.data[0] = 'x';
		msg.data[1] = 0x02;

		tempByte = hmiKeypad->GetButtonState(ButtonNames_Crane);
		tempByte |= (hmiKeypad->GetButtonState(ButtonNames_WorkLightLeft) << 2);
		tempByte |= (hmiKeypad->GetButtonState(ButtonNames_WorkLightRight) << 4);
		tempByte |= (hmiKeypad->GetButtonState(ButtonNames_StrobeLight) << 6);

		msg.data[2] = tempByte;

		tempByte = hmiKeypad->GetButtonState(ButtonNames_Compressor);
		tempByte |= (hmiKeypad->GetButtonState(ButtonNames_DomeLight) << 2);
		tempByte |= (hmiKeypad->GetButtonState(ButtonNames_CraneLight) << 4);
		tempByte |= (hmiKeypad->GetButtonState(ButtonNames_Aux) << 6);

		msg.data[3] = tempByte;

		dispEventWithData(EVENT_CAN2_TX, (uint32_t *)&msg, sizeof(msg));


		msg.dlc = 6;
		msg.data[0] = 'x';
		msg.data[1] = 0x03;

		tempByte = appOutputs->getOutputState(HC_Outputs_CranePower);
		tempByte |= (appOutputs->getOutputState(HC_Outputs_StrobeLights) << 2);
		tempByte |= (appOutputs->getOutputState(HC_Outputs_WorkLightsLeft) << 4);
		tempByte |= (appOutputs->getOutputState(HC_Outputs_WorkLightsRight) << 6);

		msg.data[2] = tempByte;

		tempByte = appOutputs->getOutputState(HC_Outputs_CompartmentLights);
		tempByte |= (appOutputs->getOutputState(HC_Outputs_CompressorPowerUnit) << 2);
		tempByte |= (appOutputs->getOutputState(HC_Outputs_Aux) << 4);
		tempByte |= (appOutputs->getOutputState(OD_Outputs_CraneLights) << 6);

		msg.data[3] = tempByte;

		tempByte = appOutputs->getOutputState(LC_Outputs_1);
		tempByte |= (appOutputs->getOutputState(LC_Outputs_2) << 2);
		tempByte |= (appOutputs->getOutputState(LC_Outputs_3) << 4);
		tempByte |= (appOutputs->getOutputState(LC_Outputs_4) << 6);

		msg.data[4] = tempByte;

		tempByte = appOutputs->getOutputState(LC_Outputs_5);
		tempByte |= (appOutputs->getOutputState(LC_Outputs_6) << 2);
		tempByte |= (appOutputs->getOutputState(LC_Outputs_7) << 4);

		msg.data[5] = tempByte;

		dispEventWithData(EVENT_CAN2_TX, (uint32_t *)&msg, sizeof(msg));

		msg.dlc = 4;
		msg.data[0] = 'x';
		msg.data[1] = 0x04;

		tempByte = appOutputs->LC_getConfiguredType(0);
		tempByte |= (appOutputs->LC_getConfiguredType(1) << 2);
		tempByte |= (appOutputs->LC_getConfiguredType(2) << 4);
		tempByte |= (appOutputs->LC_getConfiguredType(3) << 6);

		msg.data[2] = tempByte;

		tempByte = appOutputs->LC_getConfiguredType(4);
		tempByte |= (appOutputs->LC_getConfiguredType(5) << 2);
		tempByte |= (appOutputs->LC_getConfiguredType(6) << 4);

		msg.data[3] = tempByte;

		dispEventWithData(EVENT_CAN2_TX, (uint32_t *)&msg, sizeof(msg));

    }
    else if(count == 100)
    {
    	count = 0;

		msg.dlc = 8;
		msg.data[0] = 'x';
		msg.data[1] = 0x05;

		uint32_t tempCurrent = appOutputs->getOutputCurrent(HC_Outputs_CranePower);

		msg.data[2] = ( (tempCurrent >> 16) & 0xff );
		msg.data[3] = ( (tempCurrent >> 8) & 0xff );
		msg.data[4] = ( tempCurrent & 0xff );

		tempCurrent = appOutputs->getOutputCurrent(HC_Outputs_WorkLightsLeft);

		msg.data[5] = ( (tempCurrent >> 16) & 0xff );
		msg.data[6] = ( (tempCurrent >> 8) & 0xff );
		msg.data[7] = ( tempCurrent & 0xff );

		dispEventWithData(EVENT_CAN2_TX, (uint32_t *)&msg, sizeof(msg));

		msg.dlc = 8;
		msg.data[0] = 'x';
		msg.data[1] = 0x06;

		tempCurrent = appOutputs->getOutputCurrent(HC_Outputs_WorkLightsRight);

		msg.data[2] = ( (tempCurrent >> 16) & 0xff );
		msg.data[3] = ( (tempCurrent >> 8) & 0xff );
		msg.data[4] = ( tempCurrent & 0xff );

		tempCurrent = appOutputs->getOutputCurrent(HC_Outputs_StrobeLights);

		msg.data[5] = ( (tempCurrent >> 16) & 0xff );
		msg.data[6] = ( (tempCurrent >> 8) & 0xff );
		msg.data[7] = ( tempCurrent & 0xff );

		dispEventWithData(EVENT_CAN2_TX, (uint32_t *)&msg, sizeof(msg));

		msg.dlc = 8;
		msg.data[0] = 'x';
		msg.data[1] = 0x07;

		tempCurrent = appOutputs->getOutputCurrent(HC_Outputs_CompressorPowerUnit);

		msg.data[2] = ( (tempCurrent >> 16) & 0xff );
		msg.data[3] = ( (tempCurrent >> 8) & 0xff );
		msg.data[4] = ( tempCurrent & 0xff );

		tempCurrent = appOutputs->getOutputCurrent(HC_Outputs_CompartmentLights);

		msg.data[5] = ( (tempCurrent >> 16) & 0xff );
		msg.data[6] = ( (tempCurrent >> 8) & 0xff );
		msg.data[7] = ( tempCurrent & 0xff );

		dispEventWithData(EVENT_CAN2_TX, (uint32_t *)&msg, sizeof(msg));

		msg.dlc = 8;
		msg.data[0] = 'x';
		msg.data[1] = 0x08;

		tempCurrent = appOutputs->getOutputCurrent(HC_Outputs_Aux);

		msg.data[2] = ( (tempCurrent >> 16) & 0xff );
		msg.data[3] = ( (tempCurrent >> 8) & 0xff );
		msg.data[4] = ( tempCurrent & 0xff );

		tempCurrent = appOutputs->getOutputCurrent(LC_Outputs_1);

		msg.data[5] = ( (tempCurrent >> 16) & 0xff );
		msg.data[6] = ( (tempCurrent >> 8) & 0xff );
		msg.data[7] = ( tempCurrent & 0xff );

		dispEventWithData(EVENT_CAN2_TX, (uint32_t *)&msg, sizeof(msg));


    }

}

const uint32_t CANRxId = 0x00ef3800;
const uint32_t CANRxMask = 0x00fff800;
void eventCANParseSystem()
{
    uint32_t rxId = CANRxId + (controllerAddress << 8);
    uint32_t xmitId = CANBaseAddress + controllerAddress;
    CANEventMsg_t msgs[CAN_TX_QCOUNT];
    int len = dispGetQData(EVENT_CAN2_PARSESYSTEM, (uint32_t *)&msgs);
    len /= (sizeof(msgs[0]) / sizeof(uint32_t));
    for (int i = 0; i < len; ++i)
    {
        if ((msgs[i].canId & CANRxMask) == rxId)
        {
            CANEventMsg_t *msg = &(msgs[i]);
            if( ! CANCoreProcessStandardMessages(xmitId, msg))
            {
                // process your messages here
                if ((msg->data[0] == 'G') && (ConfigLocker.isUnlocked()))
                {
                    int bsize = msg->dlc - 2;
                    if (bsize > 0)
                    {
                        bool writeIt = false;
                        uint8_t param = msg->data[1];
                        uint16_t val = *((uint16_t *)(&(msg->data[2])));
                        if (param == 1)   // Compressor Timeout
                        {
                            eeCfgObj.setCfgCompressorTimeout(val);
                            writeIt = true;
                        }
                        else if (param == 2) // Crane Timeout
                        {
                            eeCfgObj.setCfgCraneTimeout(val);
                            writeIt = true;
                        }
                        else if (param == 3) // Ignition Activate Time
                        {
                            eeCfgObj.setCfgIgnitionActTime(val);
                            writeIt = true;
                        }
                        else if ((param >= 4) && (param <= 11)) // HC current limits
                        {
                            eeCfgObj.setCfgHCCurrentLimit((param-4), val);
                            writeIt = true;
                        }

                        if (writeIt) eeCfgObj.writeBrdCfg();
                    }
                }
            }
        }
    }
}

const uint32_t CANRxId_keypad = 0x01F20d00;
const uint32_t CANRxMask_keypad = 0x01FFFF00;
void eventCANParseKeypad()
{
	CANEventMsg_t msg[CAN_TX_QCOUNT];
	int len = dispGetQData(EVENT_CAN2_PARSEKEYPAD, (uint32_t *)&msg);
	len /= (sizeof(msg[0]) / sizeof(uint32_t));
	if(len != 0)
	{
		if((msg[0].canId & CANRxMask_keypad) == CANRxId_keypad)
		{
			hmiKeypad->ParseKeypadMessage(msg[0].data);
		}
	}
}

const uint32_t CANRxId_crane = 0x00FFAA00;
const uint32_t CANRxMask_crane = 0x01FFFF00;
void eventCANParseCrane()
{
	CANEventMsg_t msg[CAN_TX_QCOUNT];
	int len = dispGetQData(EVENT_CAN2_PARSECRANE, (uint32_t *)&msg);
	len /= (sizeof(msg[0]) / sizeof(uint32_t));
	if(len != 0)
	{
		if((msg[0].canId & CANRxMask_crane) == CANRxId_crane)
		{
			//msg[0].data[0]; // Capacity
			//msg[0].data[1]; // Boom Angle
			//msg[0].data[2]; // Hoist Speed
			//msg[0].data[3]; // Lift Speed

			//msg[0].data[4]; // Slew Speed
			//msg[0].data[5]; // Extension Speed
			//msg[0].data[6]; // Outriggers Status (bit 1-4), Auxillary Status (bit 5-6), High Idle Status (bit 7-8)
			//msg[0].data[7]; // Engine Start Status (bit 1-2), Engine Stop Status (bit 3-4), Crane Active Status (bit 5-6)

			uint8_t tempByte = msg[0].data[6];
			tempByte = (tempByte & 0x30) >> 4;

			//when canCraneAux changes, "press" compressor button
			if((tempByte == 0x01) != canCraneAux)
			{
				canCraneAux = (tempByte == 0x01);

				//if canCraneAux is high and compressor is off, turn it on
				// if canCraneAux is low and compressor is on, turn it off
				// this keeps physical button and CAN message in sync
				if(canCraneAux)
				{
					if(hmiKeypad->GetButtonState(ButtonNames_Compressor) == ButtonState_InActive)
					{
						hmiKeypad->AuxillaryButtonPress(ButtonNames_Compressor);
					}
				}
				else
				{
					if(hmiKeypad->GetButtonState(ButtonNames_Compressor) == ButtonState_Active)
					{
						hmiKeypad->AuxillaryButtonPress(ButtonNames_Compressor);
					}
				}
			}

			tempByte = msg[0].data[7];
			tempByte = (tempByte & 0x03);
			canCraneEngineStart = (tempByte == 0x01);

			tempByte = msg[0].data[7];
			tempByte = (tempByte & 0x0C) >> 2;
			canCraneEngineStop = (tempByte == 0x01);

			tempByte = msg[0].data[7];
			tempByte = (tempByte & 0x30) >> 4;
			canCraneActiveStatus = (tempByte == 0x01);
		}
	}
}

/************* CANParameter functions ***********************************/
bool CANParameter::isValid()
{
    return(((uwTick - lastTimeTick) < validTimeTicks) && (val != 0xffffffff));
}
void CANParameter::setVal(uint8_t *data, uint8_t bytePos, uint8_t numBytes)
{
    if ((numBytes > 4) || (numBytes < 1)) return;


    uint32_t v = 0;
    --bytePos;
    bool allfs = true;
    for (int i = 0; i < numBytes; ++i)
    {
        if (data[bytePos + i] != 0xff)
        {
            allfs = false;
        }
        ((uint8_t *)(&v))[i] = data[bytePos + i];
    }
    if (allfs) val = 0xffffffff;
    else
    {
        val = v;
        lastTimeTick = uwTick;
    }
}


#ifdef __cplusplus
}
#endif

