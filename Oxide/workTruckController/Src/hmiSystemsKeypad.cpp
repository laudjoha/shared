
#include <globals.h>
#include <hmiSystemsKeypad.h>
#include <ApplicationOutputs.h>
#include <dispatcherSetup.h>


hmiSystemsKeypad::hmiSystemsKeypad(uint8_t _sourceAddress, uint8_t _rows, uint8_t _columns)
{
	sourceAddress = _sourceAddress;
	rows = _rows;
	columns = _columns;

	uint8_t tempButtons = rows * columns;
	for(int i = 0; i < tempButtons; i++)
	{
		prevButtonState.push_back(ButtonState_InActive);
		buttonState.push_back(ButtonState_InActive);
	}

	txMessage.canId = 0x01F20E00;
	txMessage.isExt = 1;
	txMessage.dlc = 8;

	txMessage.data[0] = 0xFF;
	txMessage.data[1] = 0x00;
	txMessage.data[2] = 0x00;
	txMessage.data[3] = 0x00;
	txMessage.data[4] = 0x00;
	txMessage.data[5] = 0x00;
	txMessage.data[6] = 0x00;
	txMessage.data[7] = 0x07;

	dispEventWithData(EVENT_CAN2_TX, (uint32_t *)&txMessage, sizeof(txMessage));
}

hmiSystemsKeypad::~hmiSystemsKeypad()
{}

CANEventMsg_t hmiSystemsKeypad::GetTxMessage()
{
	return txMessage;
}

ButtonState_t hmiSystemsKeypad::GetButtonState(ButtonNames_t button)
{
	return buttonState[button];
}

void hmiSystemsKeypad::SetButtonState(ButtonNames_t button, ButtonState_t state)
{
	buttonState[button] = state;
}


void hmiSystemsKeypad::SetLEDState(Application_Outputs_t output, ButtonState_t state)
{
	if(output > HC_Outputs_MAX)
	{
		return;
	}

	switch(output)
	{
		case HC_Outputs_CranePower:
			SetButtonState(ButtonNames_Crane, state);
			break;
		case HC_Outputs_StrobeLights:
			SetButtonState(ButtonNames_StrobeLight, state);
			break;
		case HC_Outputs_WorkLightsLeft:
			SetButtonState(ButtonNames_WorkLightLeft, state);
			break;
		case HC_Outputs_WorkLightsRight:
			SetButtonState(ButtonNames_WorkLightRight, state);
			break;
		case HC_Outputs_CompartmentLights:
			SetButtonState(ButtonNames_DomeLight, state);
			break;
		case HC_Outputs_CompressorPowerUnit:
			SetButtonState(ButtonNames_Compressor, state);
			break;
		case HC_Outputs_Aux:
			SetButtonState(ButtonNames_Aux, state);
			break;

		default:
			break;
	}
}

void hmiSystemsKeypad::ParseKeypadMessage(unsigned char data[])
{
	//byte 0 is always 0xff, button data is in bytes 1-6
	for(int i = 1; i < 7; i++)
	{
		//loop over buttons. each button is two bits
		for(int j = 0; j < 4; j++)
		{
			uint8_t testBit = 0x01 << (j * 2);
			uint8_t buttonIndex = ((i - 1) * 4) + j; //get position in zero based prevButtonState vector

			if( (data[i] & testBit) == testBit)
			{
				//button is pressed when testBit is set

				if(prevButtonState[buttonIndex] == ButtonState_InActive)
				{
					//button has changed state since last message
					prevButtonState[buttonIndex] = ButtonState_Active;
				}
			}
			else
			{
				//button is not pressed testBit is clear

				if(prevButtonState[buttonIndex] == ButtonState_Active)
				{
					//button had previously been pressed
					// this means button has been released
					prevButtonState[buttonIndex] = ButtonState_InActive;
					ToggleButton(buttonIndex);
				}
			}
		}
	}
}

void hmiSystemsKeypad::AuxillaryButtonPress(ButtonNames_t button)
{
	uint8_t index = NameToIndex(button);

	ToggleButton(index);
}

void hmiSystemsKeypad::UpdateKeypadLights()
{
	if(eStopActive)
	{
		buttonState[0] = ButtonState_InActive;
		buttonState[1] = ButtonState_InActive;
		buttonState[2] = ButtonState_InActive;
		buttonState[3] = ButtonState_InActive;
		buttonState[4] = ButtonState_InActive;
		buttonState[5] = ButtonState_InActive;
		buttonState[6] = ButtonState_InActive;
		buttonState[7] = ButtonState_InActive;
	}

	uint8_t tempByte = 0x00;
	tempByte |= buttonState[0];
	tempByte |= buttonState[1] << 2;
	tempByte |= buttonState[2] << 4;
	tempByte |= buttonState[3] << 6;

	txMessage.data[1] = tempByte;

	tempByte = 0x00;
	tempByte |= buttonState[4];
	tempByte |= buttonState[5] << 2;
	tempByte |= buttonState[6] << 4;
	tempByte |= buttonState[7] << 6;

	txMessage.data[2] = tempByte;

	dispEventWithData(EVENT_CAN2_TX, (uint32_t *)&txMessage, sizeof(txMessage));
}

void hmiSystemsKeypad::ToggleButton(uint8_t buttonIndex)
{
	if(eStopActive)
	{
		return;
	}

	if(buttonState[buttonIndex] == ButtonState_Active)
	{
		buttonState[buttonIndex] = ButtonState_InActive;

		switch(buttonIndex)
		{
			case 0:
				appOutputs->OutputSet_Lo(HC_Outputs_CranePower);
				break;

			case 1:
				appOutputs->OutputSet_Lo(HC_Outputs_WorkLightsLeft);
				break;

			case 2:
				appOutputs->OutputSet_Lo(HC_Outputs_WorkLightsRight);
				break;

			case 3:
				appOutputs->OutputSet_Lo(HC_Outputs_StrobeLights);
				break;

			case 4:
				appOutputs->OutputSet_Lo(HC_Outputs_CompressorPowerUnit);
				break;

			case 5:
				appOutputs->OutputSet_Lo(HC_Outputs_CompartmentLights);
				break;

			case 6:
				appOutputs->OD_setLo(0);
				break;

			case 7:
				appOutputs->OutputSet_Lo(HC_Outputs_Aux);
				break;
		}
	}
	else if(buttonState[buttonIndex] == ButtonState_InActive)
	{
		buttonState[buttonIndex] = ButtonState_Active;

		switch(buttonIndex)
		{
			case 0:
				appOutputs->OutputSet_Hi(HC_Outputs_CranePower);
				break;

			case 1:
				appOutputs->OutputSet_Hi(HC_Outputs_WorkLightsLeft);
				break;

			case 2:
				appOutputs->OutputSet_Hi(HC_Outputs_WorkLightsRight);
				break;

			case 3:
				appOutputs->OutputSet_Hi(HC_Outputs_StrobeLights);
				break;

			case 4:
				appOutputs->OutputSet_Hi(HC_Outputs_CompressorPowerUnit);
				break;

			case 5:
				appOutputs->OutputSet_Hi(HC_Outputs_CompartmentLights);
				break;

			case 6:
				appOutputs->OD_setHi(0);
				break;

			case 7:
				appOutputs->OutputSet_Hi(HC_Outputs_Aux);
				break;
		}
	}
	else
	{
		//buttonState == ButtonState_Fault and we want to ignore the press
		buttonState[buttonIndex] = ButtonState_InActive;
	}

	UpdateKeypadLights();
}


ButtonNames_t hmiSystemsKeypad::IndexToName(uint8_t buttonIndex)
{
	ButtonNames_t buttonName = ButtonNames_MAX;

	switch(buttonIndex)
	{
		case 0:
			buttonName = ButtonNames_Crane;
			break;
		case 1:
			buttonName = ButtonNames_WorkLightLeft;
			break;
		case 2:
			buttonName = ButtonNames_WorkLightRight;
			break;
		case 3:
			buttonName = ButtonNames_StrobeLight;
			break;
		case 4:
			buttonName = ButtonNames_Compressor;
			break;
		case 5:
			buttonName = ButtonNames_DomeLight;
			break;
		case 6:
			buttonName = ButtonNames_CraneLight;
			break;
		case 7:
			buttonName = ButtonNames_Aux;
			break;

		default:
			break;
	}

	return buttonName;
}



uint8_t hmiSystemsKeypad::NameToIndex(ButtonNames_t buttonName)
{
	uint8_t buttonIndex = rows * columns + 1;

	switch(buttonName)
	{
		case ButtonNames_Crane:
			buttonIndex = 0;
			break;
		case ButtonNames_WorkLightLeft:
				buttonIndex = 1;
				break;
		case ButtonNames_WorkLightRight:
				buttonIndex = 2;
				break;
		case ButtonNames_StrobeLight:
				buttonIndex = 3;
				break;
		case ButtonNames_Compressor:
				buttonIndex = 4;
				break;
		case ButtonNames_DomeLight:
				buttonIndex = 5;
				break;
		case ButtonNames_CraneLight:
				buttonIndex = 6;
				break;
		case ButtonNames_Aux:
				buttonIndex = 7;
				break;

		default:
			break;
	}

	return buttonIndex;
}
