
#include <adc.h>
#include <utilities.h>
#include <globals.h>

extern ADC_HandleTypeDef hadc1;
//extern ADC_HandleTypeDef hadc2;

FilterAve adc2Ave(10);


/********** EVENTS **************************************************/
#ifdef __cplusplus
extern "C" {
#endif
#include <dispatcherSetup.h>

/***
void eventStartADC()
{
	if(HAL_ADC_Start_IT(&hadc1) != HAL_OK)
	{
		// Start Conversation Error
		_Error_Handler((char *)__FILE__, __LINE__);
	}
}
***/

uint16_t adcVals[ADC1VALCOUNT];
//uint16_t adc2Vals[ADC2VALCOUNT];

uint16_t adc2Vals[3];


/*
 * adcVals
 * 	1 - Analog feedback 1
 * 	2 - Analog feedback 2
 * 	3 - Analog feedback 3
 * 	4 - Analog feedback 4
 * 	5 - HW Ver
 * 	6 - Digital Tri-state 1
 * 	7 - Digital Tri-state 2
 * 	8 - Digital Tri-state mux
 * 		mux1 - Digital Tri-state 3
 * 		mux2 - Digital Tri-state 4
 * 		mux3 - Digital Tri-state 5
 * 		mux4 - Digital Tri-state 6
 * 		mux5 - Digital Tri-state 7
 * 		mux6 - Digital Tri-state 8
 * 		mux7 - Digital Tri-Flyback
 * 		mux8 - thermistor
 */

bool adcReady = false;
uint32_t currentMuxPosition = 0;
void handleMuxInput(uint32_t muxCounts)
{
	switch(currentMuxPosition)
	{
		case 5:
		{
			digitalInCounts[2] = muxCounts;

			//shift mux index, set inputs to configure mux for next position
			currentMuxPosition++;
			HAL_GPIO_WritePin(DIG_TRI_SELECT_0_uP_GPIO_Port, DIG_TRI_SELECT_0_uP_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(DIG_TRI_SELECT_1_uP_GPIO_Port, DIG_TRI_SELECT_1_uP_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(DIG_TRI_SELECT_2_uP_GPIO_Port, DIG_TRI_SELECT_2_uP_Pin, GPIO_PIN_SET);
			break;
		}
		case 10:
		{
			digitalInCounts[3] = muxCounts;

			//shift mux index, set inputs to configure mux for next position
			currentMuxPosition++;
			HAL_GPIO_WritePin(DIG_TRI_SELECT_0_uP_GPIO_Port, DIG_TRI_SELECT_0_uP_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(DIG_TRI_SELECT_1_uP_GPIO_Port, DIG_TRI_SELECT_1_uP_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(DIG_TRI_SELECT_2_uP_GPIO_Port, DIG_TRI_SELECT_2_uP_Pin, GPIO_PIN_SET);
			break;
		}
		case 15:
		{
			digitalInCounts[4] = muxCounts;

			//shift mux index, set inputs to configure mux for next position
			currentMuxPosition++;
			HAL_GPIO_WritePin(DIG_TRI_SELECT_0_uP_GPIO_Port, DIG_TRI_SELECT_0_uP_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(DIG_TRI_SELECT_1_uP_GPIO_Port, DIG_TRI_SELECT_1_uP_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(DIG_TRI_SELECT_2_uP_GPIO_Port, DIG_TRI_SELECT_2_uP_Pin, GPIO_PIN_SET);
			break;
		}
		case 20:
		{
			digitalInCounts[5] = muxCounts;

			//shift mux index, set inputs to configure mux for next position
			currentMuxPosition++;
			HAL_GPIO_WritePin(DIG_TRI_SELECT_0_uP_GPIO_Port, DIG_TRI_SELECT_0_uP_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(DIG_TRI_SELECT_1_uP_GPIO_Port, DIG_TRI_SELECT_1_uP_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(DIG_TRI_SELECT_2_uP_GPIO_Port, DIG_TRI_SELECT_2_uP_Pin, GPIO_PIN_RESET);
			break;
		}
		case 25:
		{
			digitalInCounts[6] = muxCounts;

			//shift mux index, set inputs to configure mux for next position
			currentMuxPosition++;
			HAL_GPIO_WritePin(DIG_TRI_SELECT_0_uP_GPIO_Port, DIG_TRI_SELECT_0_uP_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(DIG_TRI_SELECT_1_uP_GPIO_Port, DIG_TRI_SELECT_1_uP_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(DIG_TRI_SELECT_2_uP_GPIO_Port, DIG_TRI_SELECT_2_uP_Pin, GPIO_PIN_RESET);
			break;
		}
		case 30:
		{
			digitalInCounts[7] = muxCounts;

			//shift mux index, set inputs to configure mux for next position
			currentMuxPosition++;
			HAL_GPIO_WritePin(DIG_TRI_SELECT_0_uP_GPIO_Port, DIG_TRI_SELECT_0_uP_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(DIG_TRI_SELECT_1_uP_GPIO_Port, DIG_TRI_SELECT_1_uP_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(DIG_TRI_SELECT_2_uP_GPIO_Port, DIG_TRI_SELECT_2_uP_Pin, GPIO_PIN_RESET);
			break;
		}
		case 35:
		{
			digitalTriFlybackCounts = muxCounts;

			//shift mux index, set inputs to configure mux for next position
			currentMuxPosition++;
			HAL_GPIO_WritePin(DIG_TRI_SELECT_0_uP_GPIO_Port, DIG_TRI_SELECT_0_uP_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(DIG_TRI_SELECT_1_uP_GPIO_Port, DIG_TRI_SELECT_1_uP_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(DIG_TRI_SELECT_2_uP_GPIO_Port, DIG_TRI_SELECT_2_uP_Pin, GPIO_PIN_RESET);
			break;
		}
		case 40:
		{
			thermistorCounts = muxCounts;

			//shift mux index, set inputs to configure mux for next position
			currentMuxPosition++;
			HAL_GPIO_WritePin(DIG_TRI_SELECT_0_uP_GPIO_Port, DIG_TRI_SELECT_0_uP_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(DIG_TRI_SELECT_1_uP_GPIO_Port, DIG_TRI_SELECT_1_uP_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(DIG_TRI_SELECT_2_uP_GPIO_Port, DIG_TRI_SELECT_2_uP_Pin, GPIO_PIN_SET);

			adcReady = true;

			break;
		}
		default:
		{
			currentMuxPosition++;

			if(currentMuxPosition > 40)
			{
				currentMuxPosition = 0;
				HAL_GPIO_WritePin(DIG_TRI_SELECT_0_uP_GPIO_Port, DIG_TRI_SELECT_0_uP_Pin, GPIO_PIN_SET);
				HAL_GPIO_WritePin(DIG_TRI_SELECT_1_uP_GPIO_Port, DIG_TRI_SELECT_1_uP_Pin, GPIO_PIN_SET);
				HAL_GPIO_WritePin(DIG_TRI_SELECT_2_uP_GPIO_Port, DIG_TRI_SELECT_2_uP_Pin, GPIO_PIN_SET);
			}
			break;
		}
	}
}


void eventADCRead()
{
    // sum1 is for Analog feedback 1
	// sum2 is for Analog feedback 2
	// sum3 is for Analog feedback 3
	// sum4 is for Analog feedback 4

	// sum5 is for Hardware Version
	// sum6 is for Digital Tri-State 1
	// sum7 is for Digital Tri-State 2
	// sum8 is for Digital Tri-State mux

	uint32_t sum1 = 0;
	uint32_t sum2 = 0;
	uint32_t sum3 = 0;
	uint32_t sum4 = 0;

	uint32_t sum5 = 0;
	uint32_t sum6 = 0;
	uint32_t sum7 = 0;
	uint32_t sum8 = 0;

	uint32_t sum9 = 0;

	for (uint32_t i = 0; i < ADC1VALCOUNT; ++i)
	{
		sum1 += adcVals[i];
		sum2 += adcVals[i+1];
		sum3 += adcVals[i+2];
		sum4 += adcVals[i+3];

		sum5 += adcVals[i+4];
		sum6 += adcVals[i+5];
		sum7 += adcVals[i+6];
		sum8 += adcVals[i+7];

        sum9 += adcVals[i+8];

        //set i to last index so for loop iteration will bump it
		i = i + 8;
	}

	uint32_t ave1 = sum1 / ADC1VALCHANCOUNT;
	uint32_t ave2 = sum2 / ADC1VALCHANCOUNT;
	uint32_t ave3 = sum3 / ADC1VALCHANCOUNT;
	uint32_t ave4 = sum4 / ADC1VALCHANCOUNT;

	uint32_t ave5 = sum5 / ADC1VALCHANCOUNT;
	uint32_t ave6 = sum6 / ADC1VALCHANCOUNT;
	uint32_t ave7 = sum7 / ADC1VALCHANCOUNT;
	uint32_t ave8 = sum8 / ADC1VALCHANCOUNT;

    uint32_t ave9 = sum9 / ADC1VALCHANCOUNT;

    anfbCounts[0] = ave1;
	anfbCounts[1] = ave2;

	setHWRevFromAnalogCounts((int)ave3);

	anfbCounts[2] = ave4;

    digitalInCounts[0] = ave5;
    digitalInCounts[1] = ave6;

	if(digitalInCounts[5] > DIGITAL_IN_LOW_THRESH &&
	   digitalInCounts[5] < DIGITAL_IN_HI_THRESH)
	{
		//if estop is floating, trigger event
		dispEvent(EVENT_ESTOP);
	}
	else
	{
		eStopActive = false;
	}

    setVProtectedAnalogCounts((int)ave7);

    anfbCounts[3] = ave8;

    // ave9 = mux output

	handleMuxInput(ave9);

}



/*********** ADC HAL CALLBACKS ************************************/
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	if (hadc == &hadc1)	dispEvent(EVENT_ADC_READ);
}
/***
void HAL_ADC_ConvHalfCpltCallback(ADC_HandleTypeDef* hadc)
{
	dispEvent(EVENT_ADC_READ);
}
***/



#ifdef __cplusplus
}
#endif

