
#include <eeConfig.h>
#include <cstring>
#include <timerUtils.h>

EEConfig eeCfgObj;

///////////////////////////////////////////////////////////////////////////////////
// EEConfig class functions
///////////////////////////////////////////////////////////////////////////////////
// NOTE: must be on 16 byte boundary for page writes
#define EECFG_BLOCKOFFSET 256


#ifdef __cplusplus
extern "C" {
#endif
#include "dispatcherSetup.h"

void eventEEConfigCallback()
{
    uint32_t msg[2];
    int len = dispGetQData(EVENT_EECONFIG_CALLBACK, msg);
    if (len >= 2)
    {
        EEConfig *eeCfg = (EEConfig *)msg[0];
        eeCfg->setErrorCode(msg[1]);
        eeCfg->doNextState();
    }
}

void eventEEConfigWriteWait()
{
    uint32_t context;
    int len = dispGetQData(EVENT_EECONFIG_WRITEWAIT, &context);
    if (len > 0)
    {
        EEConfig *ee = (EEConfig *)context;
        if (ee->getState() == 1)
        {
            ee->writeBrdCfg1();
        }
        else
        {
            ee->reset();
        }
    }
}

#ifdef __cplusplus
}
#endif

EEConfig::EEConfig() :
    cfgDataGood(false),
    blockOffset(EECFG_BLOCKOFFSET),
    baseOffset(EECFG_BLOCKOFFSET),
    errorCode(EECODE_OK),
    state(0),
    busy(false),
    changed(false),
    readComplete(false)
{
    blockSize = getEven16Bytes(sizeof(EECfgLayout_t));
    block = new uint8_t[blockSize];
    memset(block, 0, blockSize);
    lastErrMsg.clear();
    includesHeader = true;
    memset((void *)&cfgData, 0, sizeof(cfgData));

    setToDefaults();

}

EEConfig::~EEConfig()
{
    if (block != NULL) delete [] block;
}

void EEConfig::setToDefaults()
{
    // defaults if no config stored
    cfgData.cfgCommonCANTimeout  = 15;
    cfgData.cfgCompressorTimeout = 120;
    cfgData.cfgCraneTimeout      = 120;
    cfgData.cfgIgnitionActTime   = 5;
    cfgData.cfgHCCurrentLimits   = 0x66666666;      // all 15A
    changed = true;
}

void EEConfig::doNextState()
{
    switch (state)
    {
        case 1:
            writeBrdCfg1();
            break;
        case 2:
            writeBrdCfg2();
            break;
        case 3:
            writeBrdCfg3();
            break;
        case 4:
            writeBrdCfg4();
            break;
        case 5:
            writeBrdCfg5();
            break;
        case 10:
            readCfg10();
            break;
        default:
            reset();
            break;
    }
}

void EEConfig::reset()
{
    state = 0;
    busy = false;
}

void EEConfig::writeBrdCfg()
{
    if (busy) return;

    // read config
    blockOffset = baseOffset;
    state = 1;
    timerDelete(EVENT_EECONFIG_WRITEWAIT);
    timerSet(EVENT_EECONFIG_WRITEWAIT, 1 * TIMER_TICKSPERSECOND, TIMER_FLAG_ONESHOT, (uint32_t)this);
}

void EEConfig::writeBrdCfg1() // timeout waiting for new config requests (debounce if you will)
{
    state = 2;
    eeHandler.read((void *)this, EVENT_EECONFIG_CALLBACK);
    busy = true;
}

void EEConfig::writeBrdCfg2() // read results
{
    bool okToWrite = false;
    EECfgLayout_t *data = (EECfgLayout_t *)block;
    //if config read successful
    if (errorCode == EECODE_OK)  //tested
    {
        // set okToWrite flag
        okToWrite = true;
        lastErrMsg = "";
    }
    else
    {
        //construct config structure
        memset(block, 0, getBlockSize());
        okToWrite = true;
    }

    //if okay to write
    if (okToWrite)
    {
        //set config in structure
        data->data = cfgData;

        //write config structure
        state = 3;
        blockOffset = baseOffset;
        eeHandler.write((void *)this, EVENT_EECONFIG_CALLBACK);
    }
    else
    {
        reset();
    }
}

void EEConfig::writeBrdCfg3() // write results
{
    state = 4;
    eeHandler.write((void *)this, EVENT_EECONFIG_CALLBACK);
}

void EEConfig::writeBrdCfg4() // write results
{
    state = 5;
    eeHandler.write((void *)this, EVENT_EECONFIG_CALLBACK);
}

void EEConfig::writeBrdCfg5() // write results
{
    //if write successfull
    if (errorCode == EECODE_OK) //tested
    {
        dispEvent(EVENT_EECONFIG_UPDATED);
    }
    else
    {
        lastErrMsg = "config write error";
    }

    // reset
    reset();
}

void EEConfig::readCfg()
{
    if (busy) return;

    state = 10;
    blockOffset = baseOffset;
    eeHandler.read((void *)this, EVENT_EECONFIG_CALLBACK);
    busy = true;
}

void EEConfig::readCfg10()
{
    if (errorCode == EECODE_OK)
    {
        EECfgLayout_t *ee = (EECfgLayout_t *)block;

        cfgData = ee->data;
        cfgDataGood = true;
        changed = true;
    }
    else if (errorCode == EECODE_NOBLOCKDATAFOUND)
    {
        // use default data
        cfgDataGood = true;
        changed = true;
    }

    readComplete = true;
    dispEvent(EVENT_EECONFIG_UPDATED);

    reset();
}


bool EEConfig::hasHeader()
{
    return (includesHeader);
}


// ACPDM specific config accessors
uint8_t  EEConfig::getCfgHCCurrentLimit(int idx)
{
    uint8_t retval = 0;
    if ((idx >= 0) && (idx <= 8))
    {
        retval = (cfgData.cfgHCCurrentLimits >> (idx * 4)) & 0x0000000f;
    }

    return(retval);
}

void EEConfig::setCfgHCCurrentLimit(int idx, uint16_t val)
{
    if ((val >= 0) && (val <= 6))
    {
        if ((idx >= 0) && (idx <= 7))
        {
            uint32_t newval = cfgData.cfgHCCurrentLimits;
            uint32_t mask = 0x0000000f << (idx*4);
            uint32_t tmpval = (val & 0x0000000f) << (idx*4);
            newval &= ~mask;
            newval |= tmpval;
            cfgData.cfgHCCurrentLimits = newval;
        }
    }
}




