
#include <globals.h>
#include <can.h>
#include <output.h>
#include <adc.h>

#include <ApplicationOutputs.h>
#include <hmiSystemsKeypad.h>
#include <FiniteStateMachine.h>

#include <eeConfig.h>

#ifdef __cplusplus

FiniteStateMachine* ptoStateMachine = NULL;
uint32_t ptoCurrentState = 0;

uint8_t controllerAddress = 0;
PWMOutput *LEDPWM = NULL;

ApplicationOutputs* appOutputs = NULL;

hmiSystemsKeypad* hmiKeypad = NULL;

uint32_t anfbCounts[MAX_INFINEON_IC];

uint32_t hsCurrentSenseCounts[MAX_INFINEON_IC * INFINEON_CHANNELS];

uint32_t digitalInCounts[MAX_DIGITAL_IN];

uint32_t thermistorCounts = 0;

uint32_t digitalTriFlybackCounts = 0;

bool canCraneActiveStatus = false;
bool canCraneEngineStop = false;
bool canCraneEngineStart = false;
bool canCraneAux = false;

bool eStopActive = false;

uint8_t GetDigitalInState(uint32_t counts)
{
	/*
	 * 0 - open
	 * 1 - Gnd
	 * 2 - VBatt
	 */
	if(counts < DIGITAL_IN_LOW_THRESH)
	{
		return 0x01;
	}
	else if( counts > DIGITAL_IN_HI_THRESH)
	{
		return 0x02;
	}
	else
	{
		return 0x00;
	}
}

#endif // __cplusplus

#ifdef __cplusplus
extern "C" {
#endif

uint8_t msHookCount = 0;
uint8_t msHookEnabled = 0;
void event1msHook()
{
	if(msHookEnabled)
	{
		//settle time of internal mux is 20uS with stable load
		++msHookCount;
		if(msHookCount >= 50)
		{
			msHookCount = 0;

			dispEvent(EVENT_OVERCURRENT_CHECK);

			dispEvent(EVENT_PTO_STATE_MACHINE_STEP);
		}
	}
}

void globalSetup()
{
    controllerAddress = 0;          // address offset to the base source address for outgoing controller messages
    CANInit();

    if (HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_3) != HAL_OK)
    {
      // PWM generation Error
      Error_Handler();
    }

    // ADC starts
    if(HAL_ADC_Start_DMA(&hadc1, (uint32_t*)adcVals, ADC1VALCOUNT) != HAL_OK)
    {
      //Start Conversation Error
      Error_Handler();
    }

    HWTimerChan *LEDTimer = new HWTimerChan(htim2, TIM_CHANNEL_3);
    LEDPWM = new PWMOutput(LEDTimer);
    LEDPWM->setPWM(50);

    hmiKeypad = new hmiSystemsKeypad(0, 2, 4);

    appOutputs = new ApplicationOutputs();

    appOutputs->LC_configure(0, OutputType_HighSide);
    appOutputs->LC_configure(1, OutputType_HighSide);
    appOutputs->LC_configure(2, OutputType_HighSide);
    appOutputs->LC_configure(3, OutputType_LowSide);
    appOutputs->LC_configure(4, OutputType_LowSide);
    appOutputs->LC_configure(5, OutputType_HighSide);
    appOutputs->LC_configure(6, OutputType_HighSide);

    ptoStateMachine = new FiniteStateMachine();

    msHookEnabled = 1;

    eeCfgObj.readCfg();
}


void eventEEConfigUpdated ()
{
	if(msHookEnabled)
	{
		uint8_t tempLimit = eeCfgObj.getCfgHCCurrentLimit(0);
		appOutputs->updateCurrentLimit(HC_Outputs_CranePower, tempLimit);

		tempLimit = eeCfgObj.getCfgHCCurrentLimit(1);
		appOutputs->updateCurrentLimit(HC_Outputs_StrobeLights, tempLimit);

		tempLimit = eeCfgObj.getCfgHCCurrentLimit(2);
		appOutputs->updateCurrentLimit(HC_Outputs_WorkLightsLeft, tempLimit);

		tempLimit = eeCfgObj.getCfgHCCurrentLimit(3);
		appOutputs->updateCurrentLimit(HC_Outputs_WorkLightsRight, tempLimit);

		tempLimit = eeCfgObj.getCfgHCCurrentLimit(4);
		appOutputs->updateCurrentLimit(HC_Outputs_CompartmentLights, tempLimit);

		tempLimit = eeCfgObj.getCfgHCCurrentLimit(5);
		appOutputs->updateCurrentLimit(HC_Outputs_CompressorPowerUnit, tempLimit);

		tempLimit = eeCfgObj.getCfgHCCurrentLimit(6);
		appOutputs->updateCurrentLimit(HC_Outputs_Aux, tempLimit);
	}
}


#ifdef __cplusplus
}
#endif
