
#include <PtoStates.h>

#include <globals.h>
#include <adc.h>
#include <ApplicationOutputs.h>
#include <timerUtils.h>
#include <eeConfig.h>


/*
 * ptoCurrentState
 * 0 - ptoInit
 * 1 - ptoArm
 * 2 - ptoStart
 * 3 - ptoRetry
 * 4 - ptoFault
 * 5 - ptoShutdown
 * 6 - ptoRun
 * 7 - ptoStandby
 *
 */



/*
 * ptoInit
 *
 */


bool firstStart = true;
void ptoInit::enter(FiniteStateMachine* fsm)
{
	ptoCurrentState = 0;
	 //set engine start/stop high
	// this remains high throughout, except for being momentarily pulled low during a remote start
	appOutputs->OutputSet_Hi(LC_Outputs_1);
	firstStart = true;
}

bool cranePrev = true;

void ptoInit::run(FiniteStateMachine* fsm)
{
	appOutputs->OutputSet_Hi(LC_Outputs_1);

	if(hmiKeypad->GetButtonState(ButtonNames_Crane) == ButtonState_InActive)
	{
		//if we end up back here because engine was stopped by the remote, we want a button toggle before re-starting the engine
		cranePrev = true;
	}

	bool compButtonPress = (hmiKeypad->GetButtonState(ButtonNames_Compressor) == ButtonState_Active);
	bool craneButtonPress = ((hmiKeypad->GetButtonState(ButtonNames_Crane) == ButtonState_Active) || canCraneEngineStart ) && cranePrev;

	//if compressor or crane button has been pressed, arm the pto
	if(compButtonPress || craneButtonPress || canCraneEngineStart)
	{
		fsm->setState(ptoArm::getInstance());
	}
	else
	{
		fsm->setState(ptoInit::getInstance());
	}
}

void ptoInit::exit(FiniteStateMachine* fsm)
{
	//no action on exit
}

fsmState& ptoInit::getInstance()
{
	static ptoInit singleton;
	return singleton;
}



/*
 * ptoArm
 *
 */

void ptoArm::enter(FiniteStateMachine* fsm)
{
	ptoCurrentState = 1;
}

bool startAttemptActive = false;
uint32_t armTime = 0;
void ptoArm::run(FiniteStateMachine* fsm)
{
	bool compButtonPress = (hmiKeypad->GetButtonState(ButtonNames_Compressor) == ButtonState_Active);
	bool craneButtonPress = (hmiKeypad->GetButtonState(ButtonNames_Crane) == ButtonState_Active)  || canCraneEngineStart;

	bool buttonDemand = compButtonPress || craneButtonPress;

	if(!buttonDemand)
	{
		fsm->setState(ptoShutdown::getInstance());
		return;
	}

	if(canCraneEngineStop)
	{
		cranePrev = (hmiKeypad->GetButtonState(ButtonNames_Crane) == ButtonState_InActive);
		fsm->setState(ptoShutdown::getInstance());
		return;
	}

	if(!startAttemptActive)
	{
		startAttemptActive = true;
		appOutputs->OutputSet_Hi(LC_Outputs_2); //set pto arm high
	}
	else
	{
		//leave pto arm high for 1 second
		// clear pto arm high
		// wait an additional second, and proceed to next state
		// armTime increments every 50mS

		if(armTime > 20 && armTime < 40)
		{
			appOutputs->OutputSet_Lo(LC_Outputs_2); //set pto arm low
		}

		if(armTime > 40)
		{
			fsm->setState(ptoStart::getInstance());
			return;
		}
	}

	armTime++;
}

void ptoArm::exit(FiniteStateMachine* fsm)
{
	//this will typically not be set when we exit ptoArm,
	// but a button press while pto arm is active can send us to ptoShutdown before it turns off
	appOutputs->OutputSet_Lo(LC_Outputs_2); //set pto arm Low

	startAttemptActive = false;
	armTime = 0;
}

fsmState& ptoArm::getInstance()
{
	static ptoArm singleton;
	return singleton;
}



/*
 * ptoStart
 *
 */

void ptoStart::enter(FiniteStateMachine* fsm)
{
	ptoCurrentState = 2;
}

uint32_t startTime = 0;
void ptoStart::run(FiniteStateMachine* fsm)
{
	bool compButtonPress = (hmiKeypad->GetButtonState(ButtonNames_Compressor) == ButtonState_Active);
	bool craneButtonPress = (hmiKeypad->GetButtonState(ButtonNames_Crane) == ButtonState_Active)  || canCraneEngineStart;

	bool buttonDemand = compButtonPress || craneButtonPress;

	if(!buttonDemand)
	{
		fsm->setState(ptoShutdown::getInstance());
		return;
	}

	if(canCraneEngineStop)
	{
		cranePrev = (hmiKeypad->GetButtonState(ButtonNames_Crane) == ButtonState_InActive);
		fsm->setState(ptoShutdown::getInstance());
		return;
	}

	if(startTime <= 20)
	{
		appOutputs->OutputSet_Lo(LC_Outputs_1);
	}
	else if(startTime > 20 && startTime <= 200)
	{
		appOutputs->OutputSet_Hi(LC_Outputs_1);

		//check pto pressure
		if(digitalInCounts[DigitalIn_2] > DIGITAL_IN_HI_THRESH)
		{
			fsm->setState(ptoRun::getInstance());
		}
	}
	else
	{
		//after 10 seconds, stop checking pto pressure and retry the start sequence
		fsm->setState(ptoRetry::getInstance());
		return;
	}

	startTime++;
}

void ptoStart::exit(FiniteStateMachine* fsm)
{
	startTime = 0;
}

fsmState& ptoStart::getInstance()
{
	static ptoStart singleton;
	return singleton;
}



/*
 * ptoRun
 *
 */

void ptoRun::enter(FiniteStateMachine* fsm)
{
	ptoCurrentState = 6;

	if(appOutputs->LC_getConfiguredType(3) != OutputType_LowSide)
	{
		appOutputs->LC_configure(3, OutputType_LowSide);
	}
}

uint32_t ptoRunTimer = 0;
void ptoRun::run(FiniteStateMachine* fsm)
{
	bool compButtonPress = (hmiKeypad->GetButtonState(ButtonNames_Compressor) == ButtonState_Active);
	bool craneButtonPress = (hmiKeypad->GetButtonState(ButtonNames_Crane) == ButtonState_Active)  || canCraneEngineStart;

	bool buttonDemand = compButtonPress || craneButtonPress;

	if(!buttonDemand)
	{
		fsm->setState(ptoShutdown::getInstance());
		return;
	}

	if(canCraneEngineStop)
	{
		cranePrev = (hmiKeypad->GetButtonState(ButtonNames_Crane) == ButtonState_InActive);
		fsm->setState(ptoShutdown::getInstance());
		return;
	}

	//get compressor and crane timeouts from the config
	// determine which is requested, and if both, determine which timeout is larger
	uint16_t compressorTimeout = eeCfgObj.getCfgCompressorTimeout();
	uint16_t craneTimeout = eeCfgObj.getCfgCraneTimeout();
	uint16_t ptoRunMin = 0;

	if(compButtonPress && !craneButtonPress)
	{
		ptoRunMin = compressorTimeout;
	}
	else if(!compButtonPress && craneButtonPress)
	{
		ptoRunMin = craneTimeout;
	}
	else
	{
		if(compressorTimeout > craneTimeout)
		{
			ptoRunMin = compressorTimeout;
		}
		else
		{
			ptoRunMin = craneTimeout;
		}
	}

	//if we are entering ptoRun from ptoStart, we need to wait ~5 seconds to enter high idle
	// if we are entering ptoRun from ptoStandby we do not
	if(firstStart)
	{
		//wait 5 seconds, turn on high idle
		if( ptoRunTimer > 100 )
		{
			//activate high idle by opening low side switch
			appOutputs->OutputSet_Hi(LC_Outputs_4);
			firstStart = false;
		}
	}
	else
	{
		//activate high idle by opening low side switch
		appOutputs->OutputSet_Hi(LC_Outputs_4);
	}

	//ptoRunTimer increments every 50mS
	// convert ptoRunMin from seconds to multiple of 50mS
	// run for min time before checking low idle transition conditions
	if( ptoRunTimer > ( ptoRunMin * 20))
	{
		bool demand = (digitalTriFlybackCounts > DIGITAL_IN_HI_THRESH) || canCraneActiveStatus;

		if(demand)
		{
			//activate high idle by opening low side switch
			appOutputs->OutputSet_Hi(LC_Outputs_4);
		}
		else
		{
			fsm->setState(ptoStandby::getInstance());
		}
	}

	ptoRunTimer++;
}

void ptoRun::exit(FiniteStateMachine* fsm)
{
	ptoRunTimer = 0;
}

fsmState& ptoRun::getInstance()
{
	static ptoRun singleton;
	return singleton;
}



/*
 * ptoStandby
 *
 */

void ptoStandby::enter(FiniteStateMachine* fsm)
{
	ptoCurrentState = 7;
}

void ptoStandby::run(FiniteStateMachine* fsm)
{
	bool compButtonPress = (hmiKeypad->GetButtonState(ButtonNames_Compressor) == ButtonState_Active);
	bool craneButtonPress = (hmiKeypad->GetButtonState(ButtonNames_Crane) == ButtonState_Active)  || canCraneEngineStart;

	bool buttonDemand = compButtonPress || craneButtonPress;

	if(!buttonDemand)
	{
		fsm->setState(ptoShutdown::getInstance());
		return;
	}

	if(canCraneEngineStop)
	{
		cranePrev = (hmiKeypad->GetButtonState(ButtonNames_Crane) == ButtonState_InActive);
		fsm->setState(ptoShutdown::getInstance());
		return;
	}

	bool demand = (digitalTriFlybackCounts > DIGITAL_IN_HI_THRESH) || canCraneActiveStatus; //compressor demand high or crane active

	if(demand)
	{
		fsm->setState(ptoRun::getInstance());
	}
	else
	{
		//activate low idle by closing low side switch
		appOutputs->OutputSet_Lo(LC_Outputs_4);
	}
}

void ptoStandby::exit(FiniteStateMachine* fsm)
{

}

fsmState& ptoStandby::getInstance()
{
	static ptoStandby singleton;
	return singleton;
}



/*
 * ptoRetry
 *
 */

void ptoRetry::enter(FiniteStateMachine* fsm)
{
	ptoCurrentState = 3;
}

void ptoRetry::run(FiniteStateMachine* fsm)
{
	static uint32_t retryAttempts = 0;

	if(retryAttempts < 3)
	{
		retryAttempts++;
		fsm->setState(ptoArm::getInstance());
	}
	else
	{
		retryAttempts = 0;
		fsm->setState(ptoFault::getInstance());
	}
}

void ptoRetry::exit(FiniteStateMachine* fsm)
{

}

fsmState& ptoRetry::getInstance()
{
	static ptoRetry singleton;
	return singleton;
}



/*
 * ptoFault
 *
 */

void ptoFault::enter(FiniteStateMachine* fsm)
{
	ptoCurrentState = 4;
}

void ptoFault::run(FiniteStateMachine* fsm)
{
	//VMDTC_InternalDTCActivate(0, 51002, 0, 0); //need to investigate how to replicate this (from powervision)
	fsm->setState(ptoShutdown::getInstance());
}

void ptoFault::exit(FiniteStateMachine* fsm)
{

}

fsmState& ptoFault::getInstance()
{
	static ptoFault singleton;
	return singleton;
}



/*
 * ptoShutdown
 *
 */

void ptoShutdown::enter(FiniteStateMachine* fsm)
{
	ptoCurrentState = 5;

	if(eStopActive)
	{
		appOutputs->applicationEStop();
	}

}

void ptoShutdown::run(FiniteStateMachine* fsm)
{

	if(appOutputs->LC_getConfiguredType(3) == OutputType_HighSide)
	{
		//doing this here instead of on entry gives us an additional 50mS for output to settle
		// before going active in the opposite polarity
		appOutputs->LC_configure(3, OutputType_LowSide);
		return;
	}

	static uint32_t shutdownTime = 0;

	if(shutdownTime == 0)
	{
		//the config script looks like it wants this behavior, need to validate on the demo setup
		//hold engine start/stop low for one second
		appOutputs->OutputSet_Lo(LC_Outputs_1);

		//activate low idle by closing low side switch
		appOutputs->OutputSet_Lo(LC_Outputs_4);
	}
	else if( shutdownTime > 20 && shutdownTime < 40)
	{
		appOutputs->OutputSet_Hi(LC_Outputs_1);
	}
	else if( shutdownTime > 40)
	{
		//after two seconds return to init
		shutdownTime = 0;
		fsm->setState(ptoInit::getInstance());
		return;
	}

	shutdownTime++;
}

void ptoShutdown::exit(FiniteStateMachine* fsm)
{

}

fsmState& ptoShutdown::getInstance()
{
	static ptoShutdown singleton;
	return singleton;
}
