
#include <ApplicationOutputs.h>
#include <dispatcherSetup.h>
#include <main.h>
#include <globals.h>
#include <spi.h>

ApplicationOutputs::ApplicationOutputs()
{
	SPI* spi3 = new SPI(&hspi3);
	DigitalOutput *infineonCS = new DigitalOutput(SPI_CS_Pin, SPI_CS_GPIO_Port);
	hsOuts = new InfineonManager(spi3, infineonCS);

	lsOuts[0] = new DigitalOutput(LC_OUT_OD_1_uP_Pin, LC_OUT_OD_1_uP_GPIO_Port);
	lsOuts[1] = new DigitalOutput(LC_OUT_OD_2_uP_Pin, LC_OUT_OD_2_uP_GPIO_Port);
	lsOuts[2] = new DigitalOutput(LC_OUT_OD_3_uP_Pin, LC_OUT_OD_3_uP_GPIO_Port);
	lsOuts[3] = new DigitalOutput(LC_OUT_OD_4_uP_Pin, LC_OUT_OD_4_uP_GPIO_Port);
	lsOuts[4] = new DigitalOutput(LC_OUT_OD_5_uP_Pin, LC_OUT_OD_5_uP_GPIO_Port);
	lsOuts[5] = new DigitalOutput(LC_OUT_OD_6_uP_Pin, LC_OUT_OD_6_uP_GPIO_Port);
	lsOuts[6] = new DigitalOutput(LC_OUT_OD_7_uP_Pin, LC_OUT_OD_7_uP_GPIO_Port);
	lsOuts[7] = new DigitalOutput(OD_OUT1_uP_Pin, OD_OUT1_uP_GPIO_Port);

}

ApplicationOutputs::~ApplicationOutputs()
{
	delete hsOuts;

	for(int i = 0; i < MAX_DIGITAL_OUT_CHANNELS; i++)
	{
		delete lsOuts[i];
	}
}

void ApplicationOutputs::currentLimitCompare(uint32_t infineonICIndex, uint32_t infineonCurrentSenseMuxPosition, uint32_t measuredCurrent)
{
	uint32_t currentLimit = hsOuts->getCurrentLimit(infineonICIndex, infineonCurrentSenseMuxPosition);

	Application_Outputs_t tempOutput = getApplicationOutput(infineonICIndex,infineonCurrentSenseMuxPosition);
	ApplicationOutputFaultCurrentCounts[tempOutput] = measuredCurrent;

	if(currentLimit < measuredCurrent)
	{
		hsOuts->clearOutput(infineonICIndex, infineonCurrentSenseMuxPosition);
		ApplicationOutputState[tempOutput] = OutputState_Fault;
		hmiKeypad->SetLEDState(tempOutput, ButtonState_Fault);
		hmiKeypad->UpdateKeypadLights();
	}

	return;
}

void ApplicationOutputs::updateCurrentLimit(Application_Outputs_t output, uint8_t limit)
{
	uint32_t icIndex = getICIndex(output);
	uint32_t icOutput = getICOutput(output);

	hsOuts->setCurrentLimit(icIndex, icOutput, limit);
}

Application_Outputs_t ApplicationOutputs::getApplicationOutput(uint32_t infineonICIndex, uint32_t infineonICChannel)
{
	Application_Outputs_t output = Application_Outputs_MAX;

	switch(infineonICIndex)
	{
		case 0:
		{
			if(infineonICChannel == 0 || infineonICChannel == 3)
			{
				output = HC_Outputs_Aux;
			}
			else if(infineonICChannel == 4)
			{
				//nothing hooked up here
			}
			else if(infineonICChannel == 1)
			{
				output = LC_Outputs_7;
			}
			else if(infineonICChannel == 2)
			{
				//nothing hooked up here
			}
			break;
		}
		case 1:
		{
			if(infineonICChannel == 0 || infineonICChannel == 3)
			{
				output = HC_Outputs_CompartmentLights;
			}
			else if(infineonICChannel == 4)
			{
				output = HC_Outputs_CompressorPowerUnit;
			}
			else if(infineonICChannel == 1)
			{
				output = LC_Outputs_6;
			}
			else if(infineonICChannel == 2)
			{
				output = LC_Outputs_5;
			}
			break;
		}
		case 2:
		{
			if(infineonICChannel == 0 || infineonICChannel == 3)
			{
				output = HC_Outputs_WorkLightsLeft;
			}
			else if(infineonICChannel == 4)
			{
				output = HC_Outputs_WorkLightsRight;
			}
			else if(infineonICChannel == 1)
			{
				output = LC_Outputs_4;
			}
			else if(infineonICChannel == 2)
			{
				output = LC_Outputs_3;
			}
			break;
		}
		case 3:
		{
			if(infineonICChannel == 0 || infineonICChannel == 3)
			{
				output = HC_Outputs_CranePower;
			}
			else if(infineonICChannel == 4)
			{
				output = HC_Outputs_StrobeLights;
			}
			else if(infineonICChannel == 1)
			{
				output = LC_Outputs_1;
			}
			else if(infineonICChannel == 2)
			{
				output = LC_Outputs_2;
			}
			break;
		}
	}

	return output;
}


uint32_t ApplicationOutputs::getOutputIndex(Application_Outputs_t output)
{
	uint32_t outputIndex = (uint32_t) output;

	if(output > HC_Outputs_MAX)
	{
		outputIndex = outputIndex - 1 - ( (uint32_t) HC_Outputs_MAX); //subtract additional 1 to zero base
	}

	return outputIndex;
}

uint32_t ApplicationOutputs::getICIndex(Application_Outputs_t output)
{
	uint32_t tempOutput = getOutputIndex(output);

	uint32_t icIndex = 0;

	switch(tempOutput)
	{
		case 0:
		case 1:
			icIndex = 3;
			break;

		case 2:
		case 3:
			icIndex = 2;
			break;

		case 4:
		case 5:
			icIndex = 1;
			break;

		case 6:
		case 7:
			icIndex = 0;
			break;

		default:
			icIndex = MAX_INFINEON_IC + 1; //some number that will get rejected by the driver because it is bigger than MAX_INFINEON_IC
			break;
	}

	return icIndex;
}

uint32_t ApplicationOutputs::getICOutput(Application_Outputs_t output)
{
	uint32_t icOutput = INFINEON_CHANNELS + 1;

	switch(output)
	{
		case HC_Outputs_CranePower:
		case HC_Outputs_WorkLightsLeft:
		case HC_Outputs_CompartmentLights:
		case HC_Outputs_Aux:
			icOutput = 0;
			break;

		case LC_Outputs_1:
		case LC_Outputs_4:
		case LC_Outputs_6:
		case LC_Outputs_7:
			icOutput = 1;
			break;

		case LC_Outputs_2:
		case LC_Outputs_3:
		case LC_Outputs_5:
			icOutput = 2;
			break;

		//0 and 3 are in parallel, so 4 is next
		case HC_Outputs_StrobeLights:
		case HC_Outputs_WorkLightsRight:
		case HC_Outputs_CompressorPowerUnit:
			icOutput = 4;
			break;

		default:
			break;
	}

	return icOutput;
}

void ApplicationOutputs::OutputSet_Hi(Application_Outputs_t output)
{
	if(output > HC_Outputs_MAX)
	{
		uint32_t outputIndex = getOutputIndex(output);
		if(lcOutputConfiguredType[outputIndex] == OutputType_LowSide)
		{
			lsOuts[outputIndex]->setLo();
			ApplicationOutputState[output] = OutputState_On;
			return;
		}
	}

	uint32_t icIndex = getICIndex(output);
	uint32_t icOutputIndex = getICOutput(output);

	hsOuts->setOutput(icIndex, icOutputIndex);
	ApplicationOutputState[output] = OutputState_On;
}


void ApplicationOutputs::OutputSet_Lo(Application_Outputs_t output)
{
	if(output > HC_Outputs_MAX)
	{
		uint32_t outputIndex = getOutputIndex(output);
		if(lcOutputConfiguredType[outputIndex] == OutputType_LowSide)
		{
			lsOuts[outputIndex]->setHi();
			ApplicationOutputState[output] = OutputState_Off;
			return;
		}
	}

	uint32_t icIndex = getICIndex(output);
	uint32_t icOutputIndex = getICOutput(output);

	hsOuts->clearOutput(icIndex, icOutputIndex);
	ApplicationOutputState[output] = OutputState_Off;
}


void ApplicationOutputs::ShiftCurrentSenseOutput(uint8_t position)
{
	hsOuts->shiftDCRMux(position);
}


void ApplicationOutputs::LC_configure(uint32_t channel, OutputType_t outputType)
{
	lcOutputConfiguredType[channel] = outputType;
}

OutputType_t ApplicationOutputs::LC_getConfiguredType(uint32_t channel)
{
	if(channel < MAX_DIGITAL_OUT_CHANNELS)
	{
		return lcOutputConfiguredType[channel];
	}
	else
	{
		return OutputType_NotConfigured;
	}
}


void ApplicationOutputs::OD_setHi(uint32_t channel)
{
	lsOuts[7]->setHi();
	ApplicationOutputState[OD_Outputs_CraneLights] = OutputState_On;
}

void ApplicationOutputs::OD_setLo(uint32_t channel)
{
	lsOuts[7]->setLo();
	ApplicationOutputState[OD_Outputs_CraneLights] = OutputState_Off;
}

void ApplicationOutputs::applicationEStop()
{
	hsOuts->eStopShutdown();
}

OutputState_t ApplicationOutputs::getOutputState(Application_Outputs_t output)
{
	return ApplicationOutputState[output];
}

uint32_t ApplicationOutputs::getOutputCurrent(Application_Outputs_t output)
{
	uint32_t x_curr_mA = 0;
	uint32_t y_counts = ApplicationOutputFaultCurrentCounts[output];

	uint32_t channel = getICOutput(output);

	double m = 0.0;
	double b = 0.0;

	switch(channel)
	{
		case 0:
			m = 130.0;
			b = 27.4;
			break;

		case 1:
		case 2:
			m = 544.0;
			b = 85.1;
			break;

		case 4:
			m = 65.1;
			b = 1.63;
			break;

		default:
			break;
	}

	x_curr_mA = (uint32_t) ( 1000 * ( (y_counts - b) / m )   );

	return x_curr_mA;
}


/************ ApplicationOutputs C Functions **************************************************/
/*******************************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif

void eventEStop()
{
	eStopActive = true;
}

uint8_t dcrPosition = 0;
void eventOvercurrentCheck()
{
	for(int i = 0; i < MAX_INFINEON_IC; i++)
	{
		appOutputs->currentLimitCompare(MAX_INFINEON_IC - 1 - i, dcrPosition, anfbCounts[i]);
		hsCurrentSenseCounts[dcrPosition + (i * INFINEON_CHANNELS)] = anfbCounts[i];
	}

	if(dcrPosition < (INFINEON_CHANNELS - 1)) //dcr register is zero based
	{
		dcrPosition++;
	}
	else
	{
		dcrPosition = 0;
	}

	appOutputs->ShiftCurrentSenseOutput(dcrPosition);
}


#ifdef __cplusplus
}
#endif
