
#include <string.h>
#include <dispatcherSetup.h>
#include <globals.h>

#include <InfineonManager.h>


InfineonManager::InfineonManager(SPI *hspi_, DigitalOutput *cs_)
{
	cs = cs_;
	hspi = hspi_;

	for(int i = 0; i < MAX_INFINEON_IC; i++)
	{
		digitalOutICs[i] = new Infineon( MAX_INFINEON_IC - i);
	}

	memset(wbuf, 0, sizeof(wbuf));
	memset(rbuf, 0, sizeof(rbuf));

	InitializeICs();
	ConfigureParallelOutput();
}

InfineonManager::~InfineonManager()
{
	for(int i = 0; i < MAX_INFINEON_IC; i++ )
	{
		delete digitalOutICs[i];
	}
}

uint32_t InfineonManager::getCurrentLimit(uint32_t index_IC, uint32_t index_output)
{
	if(index_IC < MAX_INFINEON_IC)
	{
		if(index_output < INFINEON_CHANNELS)
		{
			return currentLimits[(index_IC * INFINEON_CHANNELS) + index_output];
		}
	}

	return 0;
}

void InfineonManager::setCurrentLimit(uint32_t index_IC, uint32_t index_output, uint8_t limit)
{
	/*
	 * limit values
	 * 	0 - 0.0 A
	 * 	1 - 2.5 A
	 * 	2 - 5.0 A
	 * 	3 - 7.5 A
	 * 	4 - 10.0 A
	 * 	5 - 12.5 A
	 * 	6 - 15.0 A
	 */

	double limitM = 0.0;
	double limitB = 0.0;

	switch(index_output)
	{
		case 0:
			limitM = 130.0;
			limitB = 27.4;
			break;
		case 1:
		case 2:
			limitM = 544.0;
			limitB = 85.1;
			break;
		case 4:
			limitM = 65.1;
			limitB = 1.6;
			break;

		default:
			break;
	}

	double limitX = 0.0;

	switch(limit)
	{
		case 0:
		default:
			limitX = 0.0;
			break;

		case 1:
			limitX = 2.5;
			break;
		case 2:
			limitX = 5.0;
			break;
		case 3:
			limitX = 7.5;
			break;
		case 4:
			limitX = 10.0;
			break;
		case 5:
			limitX = 12.5;
			break;
		case 6:
			limitX = 15.0;
	}

	double limitY = (limitM * limitX) + limitB;

	if(index_IC < MAX_INFINEON_IC)
	{
		if(index_output < INFINEON_CHANNELS)
		{
			currentLimits[(index_IC * INFINEON_CHANNELS) + index_output] = (uint32_t) limitY;

			if(index_output == 0)
			{
				//output 0 and output 3 are in parallel
				currentLimits[(index_IC * INFINEON_CHANNELS) + 3] = (uint32_t) limitY;
			}
		}
	}
}


void InfineonManager::InitializeICs()
{
	BuildWriteBuffer();
	isWrite = true;

	hspi->write(wbuf, MAX_INFINEON_IC, EVENT_INFINEON_CALLBACK, cs, (void *)this);
}

void InfineonManager::ConfigureParallelOutput()
{
	for(int i = 0; i < MAX_INFINEON_IC; i++)
	{
		//configure all of them to have OUT0 and OUT3 in parallel
		digitalOutICs[i]->configureParallelOutput(0x01);
	}

	BuildWriteBuffer();
	isWrite = true;
	getResponse = true;//need to change logic of this so we don't have to set it when starting a sequence

	hspi->write(wbuf, MAX_INFINEON_IC, EVENT_INFINEON_CALLBACK, cs, (void *)this);
}

void InfineonManager::setOutput(uint32_t index_IC, uint32_t index_output)
{
	if(index_IC < MAX_INFINEON_IC)
	{
		if(index_output < 5)
		{
			digitalOutICs[index_IC]->clearOrSetOutput(index_output, 1); //1 sets, 0 clears
			BuildWriteBuffer();
			isWrite = true;
			getResponse = true;

			hspi->write(wbuf, MAX_INFINEON_IC, EVENT_INFINEON_CALLBACK, cs, (void *)this);
		}
	}
}

void InfineonManager::clearOutput(uint32_t index_IC, uint32_t index_output)
{
	if(index_IC < MAX_INFINEON_IC)
	{
		if(index_output < 5)
		{
			digitalOutICs[index_IC]->clearOrSetOutput(index_output, 0); //1 sets, 0 clears
			BuildWriteBuffer();
			isWrite = true;
			getResponse = true;

			hspi->write(wbuf, MAX_INFINEON_IC, EVENT_INFINEON_CALLBACK, cs, (void *)this);
		}
	}
}

void InfineonManager::eStopShutdown()
{
	memset(wbuf, 0x80, sizeof(wbuf));

	hspi->write(wbuf, MAX_INFINEON_IC, EVENT_INFINEON_CALLBACK, cs, (void *)this);

	for(int i = 0; i < MAX_INFINEON_IC; i++)
	{
		digitalOutICs[i]->clearOutputEStop();
	}

}

void InfineonManager::shiftDCRMux(uint8_t position)
{
	for(int i = 0; i < MAX_INFINEON_IC; i++)
	{
		digitalOutICs[i]->shiftDCRMuxRegister(position);
	}

	BuildWriteBuffer();
	isWrite = true;
	getResponse = true;

	hspi->write(wbuf, MAX_INFINEON_IC, EVENT_INFINEON_CALLBACK, cs, (void *)this);
}

void InfineonManager::BuildWriteBuffer()
{
	for(int i = 0; i < MAX_INFINEON_IC; i++)
	{
		wbuf[i] = digitalOutICs[i]->getWriteByte();
	}
}

void InfineonManager::processResponse()
{
	getResponse = false;

	for(int i = 0; i < MAX_INFINEON_IC; i++)
	{
		digitalOutICs[i]->setReadByte(rbuf[i]);
		digitalOutICs[i]->step(); //updates writeByte, assigns handler state function pointer

		if(getResponse == false)
		{
			getResponse = digitalOutICs[i]->getNeedResponse();
		}
	}

	BuildWriteBuffer();

	isWrite = true;
	hspi->write(wbuf, MAX_INFINEON_IC, EVENT_INFINEON_CALLBACK, cs, (void *)this);
}

void InfineonManager::spiDone()
{
	if(getResponse)
	{
		if(isWrite)
		{
			isWrite = false;
			hspi->readWrite(wbuf, rbuf, MAX_INFINEON_IC, EVENT_INFINEON_CALLBACK, cs, (void *)this);
		}
		else
		{
			processResponse();
		}
	}
}



/************ InfineonManager C Functions **************************************************/
/*******************************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif

void eventInfineonCallback()
{
	uint32_t data;
	int count = dispGetQData(EVENT_INFINEON_CALLBACK, &data);
	if(count >= 1)
	{
		InfineonManager *inf = (InfineonManager *)data;
		inf->spiDone();
	}
}


#ifdef __cplusplus
}
#endif
