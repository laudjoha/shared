
#include <Infineon.h>


Infineon::Infineon(uint8_t _index)
{
	index = _index;
	fsmptr = &Infineon::stateInitCheckWrnDiag;
}

uint8_t Infineon::getWriteByte()
{
	return writeByte;
}

void Infineon::setReadByte(uint8_t _readByte)
{
	readByte = _readByte;
}

bool Infineon::getNeedResponse()
{
	return needResponse;
}

void Infineon::step()
{
	(this->*fsmptr)();
}

void Infineon::clearOutputEStop()
{
	//don't have a great way to re-apply previous setting once E stop clears, so just blow it out
	outputConfiguration = 0x80;
}

void Infineon::configureParallelOutput(uint8_t _setting)
{
	if(_setting > 0x03)
	{
		return;
	}

	parallelConfiguration = _setting;

	stateConfigurePCS();
}

void Infineon::clearOrSetOutput(uint32_t outputIndex, uint32_t state)
{
	uint32_t indexNibble = 0x00;
	switch(outputIndex)
	{
		case 0x00:
		{
			indexNibble |= 0x01;

			if(parallelConfiguration & 0x01) //if 0 and 3 are in parallel, also set 3
			{
				indexNibble |= 0x08;
			}

			break;
		}
		case 0x01:
		{
			indexNibble |= 0x02;

			if(parallelConfiguration & 0x02) //if 1 and 2 are in parallel, also set 2
			{
				indexNibble |= 0x04;
			}

			break;
		}
		case 0x02:
		{
			indexNibble |= 0x04;

			if(parallelConfiguration & 0x02) //if 1 and 2 are in parallel, also set 1
			{
				indexNibble |= 0x02;
			}

			break;
		}
		case 0x03:
		{
			indexNibble |= 0x08;

			if(parallelConfiguration & 0x01) //if 0 and 3 are in parallel, also set 1
			{
				indexNibble |= 0x01;
			}

			break;
		}
		case 0x04:
		{
			indexNibble |= 0x10;
			//this is the pin that drives the external FET, so it can't be in parallel with anything
			break;
		}
		default:
			return;
	}

	switch(state)
	{
		case 0x00:
		{
			outputConfiguration &= ~(indexNibble);
			break;
		}
		case 0x01:
		{
			outputConfiguration |= indexNibble;
			break;
		}
		default:
			return;
	}

	stateWriteOutRegister();
}

void Infineon::shiftDCRMuxRegister(uint8_t position)
{
	dcrMuxConfiguration = position;
	stateShiftDCRMux();
}


//
// state functions interact heavily with the read/readwrite mechanic and event handler in InfineonManager
//
void Infineon::stateConfigurePCS()
{
	if(DCRSWRbitSet)
	{
		uint8_t testByte = readByte & 0xD0;
		if(testByte == 0xD0)
		{
			uint8_t parallelSetting = (readByte & 0x0C) >> 2;
			if(parallelSetting != parallelConfiguration)
			{
				writeByte = (0xD0 | (parallelConfiguration << 2));
				needResponse = true;
				fsmptr = &Infineon::stateConfigurePCS;

			}
			else
			{
				//setting complete, clear DCR bit
				// set callback to idle so stateToggleDCR will go there when it completes
				callback = &Infineon::stateIdle;
				stateToggleDCR();
			}
		}
		else
		{
			writeByte = 0x0D;
			needResponse = true;
			fsmptr = &Infineon::stateConfigurePCS;
		}
	}
	else
	{
		callback = &Infineon::stateConfigurePCS;
		stateToggleDCR();
	}
}

void Infineon::stateToggleDCR()
{
	uint8_t testByte = readByte & 0xF0;
	if(testByte == 0xF0)
	{
		writeByte = readByte;

		uint8_t dcrBit = readByte & 0x08;
		if(dcrBit == 0x08)
		{
			//bit is set, need to clear it
			writeByte &= ~0x08;
			DCRSWRbitSet = false;
		}
		else
		{
			//bit is clear, need to set it
			writeByte |= 0x08;
			DCRSWRbitSet = true;
		}

		needResponse = true;
		fsmptr = callback;
	}
	else
	{
		writeByte = 0x07; //0x07 reads the DCR register
		needResponse = true;
		fsmptr = &Infineon::stateToggleDCR;
	}
}

void Infineon::stateInitCheckWrnDiag()
{
	uint8_t testByte = readByte & 0x40;
	if(testByte == 0x40)
	{
		uint8_t diagnosticNibble = readByte & 0x0F;
		if(diagnosticNibble == 0x00)
		{
			stateInitCheckStdDiag();
		}
		else
		{
			dispEvent(EVENT_TEST);
		}
	}
	else
	{
		writeByte = 0x01;
		needResponse = true;
		fsmptr = &Infineon::stateInitCheckWrnDiag;
	}
}

void Infineon::stateInitCheckStdDiag()
{
	uint8_t testByte = readByte & 0xF0;
	if(testByte == 0x00)
	{
		uint8_t diagnosticByte = readByte & 0x3F;
		if(diagnosticByte == 0x00)
		{
			stateInitCheckErrDiag();
		}
		else if(diagnosticByte == 0x02)
		{
			//STDDiag.SBM (switch bypass monitor) is set
			// this happens when the voltage on the output pin mapped to the mux
			// is greater than the voltage on the pad of the IC
			callback = &Infineon::stateInitCheckStdDiag;
			stateInitCheckDCR();
		}
		else if(diagnosticByte == 0x04)
		{
			//STDDiag.SLP is set
			// the chip is asleep. This is usually because the DCR mux is not configured
			callback = &Infineon::stateInitCheckStdDiag;
			stateInitCheckDCR();
		}
		else
		{
			dispEvent(EVENT_TEST);
		}
	}
	else
	{
		writeByte = 0x02;
		needResponse = true;
		fsmptr = &Infineon::stateInitCheckStdDiag;
	}
}

void Infineon::stateInitCheckDCR()
{
	uint8_t testByte = readByte & 0xF0;
	if(testByte == 0xF0)
	{
		uint8_t diagnosticNibble = readByte & 0x07;
		if(diagnosticNibble != 0x06)
		{
			writeByte = 0xF6; //set dcr mux to high impedance
			needResponse = true;
			fsmptr = callback;
		}
		else
		{
			(this->*callback)();
		}
	}
	else
	{
		writeByte = 0x07;
		needResponse = true;
		fsmptr = &Infineon::stateInitCheckDCR;
	}
}

void Infineon::stateInitCheckErrDiag()
{
	uint8_t testByte = readByte & 0x40;
	if(testByte == 0x40)
	{
		uint8_t diagnosticNibble = readByte & 0x0F;
		if(diagnosticNibble == 0x00)
		{
			callback = &Infineon::stateInitConfigureHWCR;
			stateInitCheckDCR();
		}
		else
		{
			dispEvent(EVENT_TEST);
		}
	}
	else
	{
		writeByte = 0x03;
		needResponse = true;
		fsmptr = &Infineon::stateInitCheckErrDiag;
	}
}

void Infineon::stateInitConfigureHWCR()
{
	uint8_t testByte = readByte & 0xF0;
	if(testByte == 0xE0)
	{
		uint8_t diagnosticNibble = readByte & 0x04;
		if(diagnosticNibble == 0x04)
		{
			//chip is configured for AND logic between INx pin and OUT register

			writeByte = readByte;
			writeByte &= ~(0x04);
			needResponse = true;
			fsmptr = &Infineon::stateInitSetOutputs;

			//stateInitSetOutputs();
		}
		else
		{
			stateInitSetOutputs();

			//writeByte = readByte;
			//writeByte |= 0x04; // set HWCR.COL bit
			//needResponse = true;
			//fsmptr = &Infineon::stateInitSetOutputs;
		}
	}
	else
	{
		writeByte = 0x06;
		needResponse = true;
		fsmptr = &Infineon::stateInitConfigureHWCR;
	}
}

void Infineon::stateInitSetOutputs()
{
	initComplete = true;
	writeByte = 0x80; //0x9F turns everyone on, 0x80 turns everyone off.
	needResponse = false;
	fsmptr = &Infineon::stateIdle;
}

void Infineon::stateWriteOutRegister()
{
	writeByte = outputConfiguration;
	needResponse = false;
	fsmptr = &Infineon::stateIdle;
}

void Infineon::stateShiftDCRMux()
{
	uint8_t testByte = readByte & 0xF0;
	if(testByte == 0xF0)
	{
		writeByte = (readByte & 0xF8) | dcrMuxConfiguration;
		needResponse = true;
		fsmptr = &Infineon::stateIdle;
	}
	else
	{
		//read DCR register
		writeByte = 0x07;
		needResponse = true;
		fsmptr = &Infineon::stateShiftDCRMux;
	}
}

void Infineon::stateIdle()
{
	writeByte = 0x00;
	needResponse = false;
	fsmptr = &Infineon::stateIdle;
}
